
## OOO Feb 15 - March 17

[Coverage Issue](https://gitlab.com/gitlab-com/Product/-/issues/13979)

Actions for when I return:

1. [ ] Review [JTBD analysis](https://www.youtube.com/watch?v=XZGFAHLW3_M) and see what actions/outcomes may have come out of it.
1. [ ] Check back in on [PEP Feedback](https://gitlab.com/gitlab-org/gitlab/-/issues/491924), gaps/progress.
1. [ ] Check in on Camellia/Torian and progress on [UX Priorities](https://gitlab.com/gitlab-org/gitlab/-/issues/514029).
1. [ ] Re: Strategy, review the following docs and distill any value/actions for plans, then discard:
    - https://docs.google.com/document/d/16_XCYSm9xx5_lzcGZd0nBLNnLsNrsj2w9116I9_zjc4/edit?tab=t.0#heading=h.3hkvkdvx5coo
    - https://docs.google.com/document/d/17zzhx1PT_jtjFAiDCWkqStBRqUwngzAq3WS450n6U7I/edit?pli=1&tab=t.0
1. [ ] Consider python progress and any further use of [Growth L&D budget ](https://handbook.gitlab.com/handbook/people-group/learning-and-development/growth-and-development/managers-guide/)
1. [ ] Refresh, simplify, and reduce artifacts to manage
    - Close out old vision Epics
    - Consider update to 3 year roadmap Epic, close old one
    - Maybe have one epic to organize per "theme"
    - [Vuln Managemetn Workflow Automation Theme Epic](https://gitlab.com/groups/gitlab-org/-/epics/16283)
1. [ ] Revisit and refine [list of customers ](https://docs.google.com/spreadsheets/d/1N7u9BR3P1z_IH9IolhvLgQ-L2LRKyJFqmMv_xUNWIuU/edit?gid=0#gid=0)challenged by adopting PEP / migrating from compliance pipelines.
1. [ ] May be interesting to explore [this post](https://about.gitlab.com/blog/2025/01/21/secure-and-publish-python-packages-a-guide-to-ci-integration/) and see how to add leverage PEP and maybe `include` to simplify enforcement of this at scale.


## Feb 11, 2025

1. [ ] Ensure release posts are well prepared and covered. 
1. [ ] Ensure any other open coverage items are in my [coverage issue](https://gitlab.com/gitlab-com/Product/-/issues/13979).
1. [ ] Finish UX improvement MR: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/177183 
1. [ ] Consider further AppSec workflows [in this list](https://docs.google.com/document/d/16_XCYSm9xx5_lzcGZd0nBLNnLsNrsj2w9116I9_zjc4/edit?usp=sharing) and close out
1. [ ] `In progress` Explore AI prototyping tools - created many prototypes already using Claude, would like to explore Replit, using React Playground, v0, ChatGPT, and a few others
1. [ ] `In progress` Refine Organizations/Instance level management plan, review solution validation doc from Ian
1. [ ] `In progress` Refine license severity filter 
1. [ ] `In progress` Refine KEV filter
1. [ ] `In progress` Refine Warn Mode - AppSec dashboard view
1. [ ] Consider a walkthrough video of Security Policies Direction + Update 3 Year Vision Epic


## Feb 4, 2025

1. [X] `In progress` Spin down notifications research and draw conclusions from what data we have
  - https://gitlab.com/gitlab-org/ux-research/-/issues/2785
  - [Slides](https://docs.google.com/presentation/d/1CoQ17MrEu6N2IwAHGzR26F0Lc9Pqb-Vil-jQiHPHx_A/edit#slide=id.g30b147e76de_0_423)
  - [Figma Master](https://www.figma.com/board/PY2Ob75cICv9YckR3qt5cD/Problem-Validation-Exercise%3A-Secure-Section-Notifications%2FAlerts-(Main-Template)?node-id=0-1&p=f&t=4KQ1IzNOVzYPRfcP-0)
1. [ ] Consider further AppSec workflows [in this list](https://docs.google.com/document/d/16_XCYSm9xx5_lzcGZd0nBLNnLsNrsj2w9116I9_zjc4/edit?usp=sharing) and close out
1. [ ] `In progress` Explore AI prototyping tools - created many prototypes already using Claude, would like to explore Replit, using React Playground, v0, ChatGPT, and a few others
1. [ ] `In progress` Refine Organizations/Instance level management plan, review solution validation doc from Ian
1. [ ] `In progress` Refine license severity filter 
1. [ ] `In progress` Refine KEV filter
1. [ ] `In progress` Refine Warn Mode - AppSec dashboard view
1. [ ] Consider a walkthrough video of Security Policies Direction + Update 3 Year Vision Epic



## Jan 28, 2025

1. [ ] Consider further AppSec workflows [in this list](https://docs.google.com/document/d/16_XCYSm9xx5_lzcGZd0nBLNnLsNrsj2w9116I9_zjc4/edit?usp=sharing) and close out
1. [ ] `In progress` Explore AI prototyping tools - created many prototypes already using Claude, would like to explore Replit, using React Playground, v0, ChatGPT, and a few others
1. [ ] Spin down notifications research and draw conclusions from what data we have
1. [ ] `In progress` Refine Organizations/Instance level management plan, review solution validation doc from Ian
1. [ ] `In progress` Refine license severity filter 
1. [ ] `In progress` Refine KEV filter
1. [ ] `In progress` Refine Warn Mode - AppSec dashboard view


## Jan 21, 2025

1. [X] Close out [priorities](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/137828) updates
1. [ ] Consider further AppSec workflows [in this list](https://docs.google.com/document/d/16_XCYSm9xx5_lzcGZd0nBLNnLsNrsj2w9116I9_zjc4/edit?usp=sharing) and close out
1. [ ] `In progress` Explore AI prototyping tools - created many prototypes already using Claude, would like to explore Replit, using React Playground, v0, ChatGPT, and a few others
1. [ ] Spin down notifications research and draw conclusions from what data we have
1. [ ] `In progress` Refine Organizations/Instance level management plan, review solution validation doc from Ian
1. [ ] `In progress` Refine license severity filter 
1. [ ] `In progress` Refine KEV filter
1. [ ] `In progress` Refine Warn Mode - AppSec dashboard view
1. [X] Set up coverage issue
1. [X] [Product design issue](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2766) - add content for him to review


## Jan 14, 2025

1. [X] Add slides to deck - https://docs.google.com/presentation/d/1O2qz3cE0jFs6OK5A5WGs4QvZb1GUoIwjLo9mffagCWM/edit#slide=id.g30b147e76de_0_2678
1. [X] Product Roadmap Deck updates
1. [ ] Spin down notifications research and draw conclusions from what data we have
1. [ ] `Started` Organize opportunities that align with ASPM / Vuln Management / Pure AppSec workflow improvements, finish direction page updates
1. [X] Merge direction updates
1. [X] 3 customer calls planned
1. [X] Python L&D Work
1. [X] Finish work on provenance file troubleshooting tool
1. [ ] Explore AI prototyping tools


## Dec 26, 2024

Note: Short week, Dec 26 - 29

1. [X] `Started` Finish prep of breaking changes - https://gitlab.com/groups/gitlab-org/-/epics/16360
1. [ ] `Started` Organize opportunities that align with ASPM / Vuln Management / Pure AppSec workflow improvements, finish direction page updates
1. [ ] Python L&D Work
1. [ ] Organize open items for following week, review priorities from prev weeks

## Dec 17, 2024

Note: OOO Saturday until Thursday, Dec 26

1. [ ] Direction page updates
1. [ ] `In progress` Per [this OKR](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/9570#note_2199640251), review the current expected blockers for compliance pipeline migration and make sure they are well reflected there. Need to mostly reflect back the status in the OKR I believe.
1. [ ] Organize opportunities that align with ASPM / Vuln Management / Pure AppSec workflow improvements
1. [ ] Consider a more extensive blog post on migration from compliance pipelines and benefits of pipeline execution policies, perhaps also explaining direction better
1. [ ] Create a research issue to focus on vulnerability management workflow automation, engage CS / Dean to make sure we are aligned on the goals / next steps
1. [ ] Alignment with Dean
    - Put together a more official roadmap alignment / changes plan and consider next steps
    - It may help to take a pass at updating our stage direction page, even for a first draft - https://about.gitlab.com/direction/security_risk_management/ - can encourage Dean to help flesh out more of the Why aspects.
    - Review https://docs.google.com/document/d/1Hg10hkiOEtkO2y00-wyzmX6h-cEOOde7eVuE4pKAkgQ/edit?tab=t.0 
    - Per discussion with Hillary, collect things we are NOT doing that someone should do
1. [ ] Consider handoff of https://gitlab.com/gitlab-org/gitlab/-/issues/471978 - See what Sara/Or have to say

Next:

1. [ ] Likely need to close out research in https://gitlab.com/gitlab-org/ux-research/-/issues/3190 or figure out next steps 


## Dec 10, 2024

1. [X] Finish req mapping for a FR to our plans in security policies -- [Sheet](https://docs.google.com/spreadsheets/d/1ITwyBj1u4dqpy7Zw4keOy7gt2ZtNxlLzYEHyGHRDD1E/edit?gid=813406919#gid=813406919) 
1. [ ] `In progress` Per [this OKR](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/9570#note_2199640251), review the current expected blockers for compliance pipeline migration and make sure they are well reflected there. Need to mostly reflect back the status in the OKR I believe.
1. [ ] Organize opportunities that align with ASPM / Vuln Management / Pure AppSec workflow improvements
1. [ ] Consider a more extensive blog post on migration from compliance pipelines and benefits of pipeline execution policies, perhaps also explaining direction better
1. [X] Review [Add Security Analyzer Configuration design doc](https://gitlab.com/gitlab-com/content-sites/handbook/-/merge_requests/9623).
1. [ ] Create a research issue to focus on vulnerability management workflow automation, engage CS / Dean to make sure we are aligned on the goals / next steps
1. [ ] Alignment with Dean
    - Put together a more official roadmap alignment / changes plan and consider next steps
    - It may help to take a pass at updating our stage direction page, even for a first draft - https://about.gitlab.com/direction/security_risk_management/ - can encourage Dean to help flesh out more of the Why aspects.
    - Review https://docs.google.com/document/d/1Hg10hkiOEtkO2y00-wyzmX6h-cEOOde7eVuE4pKAkgQ/edit?tab=t.0 
1. [ ] Consider handoff of https://gitlab.com/gitlab-org/gitlab/-/issues/471978 - See what Sara/Or have to say

## Dec 3, 2024

1. [ ] Finish req mapping for a FR to our plans in security policies -- [Sheet](https://docs.google.com/spreadsheets/d/1ITwyBj1u4dqpy7Zw4keOy7gt2ZtNxlLzYEHyGHRDD1E/edit?gid=813406919#gid=813406919) 
1. [ ] `In progress` Per [this OKR](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/9570#note_2199640251), review the current expected blockers for compliance pipeline migration and make sure they are well reflected there. Need to mostly reflect back the status in the OKR I believe.
1. [ ] Organize opportunities that align with ASPM / Vuln Management / Pure AppSec workflow improvements
1. [X] Share Martin's Override workaround for CI variables -- docs MR is there at least, need to confirm it's merged 
1. [ ] Consider a more extensive blog post on migration from compliance pipelines and benefits of pipeline execution policies, perhaps also explaining direction better
1. [X] Review [Add Security Analyzer Configuration design doc](https://gitlab.com/gitlab-com/content-sites/handbook/-/merge_requests/9623).
1. [ ] Create a research issue to focus on vulnerability management workflow automation, engage CS / Dean to make sure we are aligned on the goals / next steps
1. [ ] Alignment with Dean
    - Create a shorter summary of my report
    - Respond to questions on the report / 1:1 notes
    - Put together a more official roadmap alignment / changes plan and consider next steps
    - It may help to take a pass at updating our stage direction page, even for a first draft - https://about.gitlab.com/direction/security_risk_management/ - can encourage Dean to help flesh out more of the Why aspects.
    - Review https://docs.google.com/document/d/1Hg10hkiOEtkO2y00-wyzmX6h-cEOOde7eVuE4pKAkgQ/edit?tab=t.0 
1. [ ] Consider handoff of https://gitlab.com/gitlab-org/gitlab/-/issues/471978 - See what Sara/Or have to say

## Nov 26, 2024

1. [ ] Create a research issue to focus on vulnerability management workflow automation, engage CS / Dean to make sure we are aligned on the goals / next steps
1. [x] Action Dean's requests - what can come off the roadmap and go to SPM, finish report, what would be lower priority, and likely what could we work on that he likely wants
1. [X] Coffee chat with Ron 
1. [ ] Review [Add Security Analyzer Configuration design doc](https://gitlab.com/gitlab-com/content-sites/handbook/-/merge_requests/9623).
1. [X] Finish / share "Security Policies Report"
1. [ ] Organize opportunities that align with ASPM / Vuln Management / Pure AppSec workflow improvements, Share Martin's Override workaround for CI variables
1. [ ] Consider a more extensive blog post on migration from compliance pipelines and benefits of pipeline execution policies, perhaps also explaining direction better
1. [ ] Per [this OKR](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/9570#note_2199640251), review the current expected blockers for compliance pipeline migration and make sure they are well reflected there.
1. [X] If I didn't get recent customer call feedback shared from prior to OOO, circle back to do this.
1. [ ] Finish req mapping for a FR to our plans in security policies -- [Sheet](https://docs.google.com/spreadsheets/d/1ITwyBj1u4dqpy7Zw4keOy7gt2ZtNxlLzYEHyGHRDD1E/edit?gid=813406919#gid=813406919) 

## Nov 22, 2024

1. [ ] Consider https://gitlab.com/groups/gitlab-org/-/epics/15869 - audit events for cases where implementing direct enforcement may not be feasible
1. [ ] Review https://gitlab.com/gitlab-com/content-sites/handbook/-/merge_requests/9623
1. [X] Release posts!
1. [ ] Finish "Security Policies Report"
1. [ ] Organize opportunities that align with ASPM / Vuln Management / Pure AppSec workflow improvements
1. [ ] Consider a more extensive blog post on migration from compliance pipelines and benefits of pipeline execution policies, perhaps also explaining direction better
1. [ ] Per [this OKR](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/9570#note_2199640251), review the current expected blockers for compliance pipeline migration and make sure they are well reflected there.
1. [ ] If I didn't get recent customer call feedback shared from prior to OOO, circle back to do this.

## Week of Nov 12, 2024

OOO this week


## Week of Nov 5, 2024

1. [X] RP for empty needs- https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/136768
1. [X] Discuss plans with Verify for required stages - share current use cases, propose FF for usage/feedback
1. [ ] Get some more research candidates booked - Notifications -- left some items for my OOO to try to secure more research candidates.
1. [X] Dogfooding meeting
1. [X] Two research interviews planned - one moved
1. [ ] Get other release posts ready
1. [ ] Finish "Security Policies Report"
1. [ ] Organize opportunities that align with ASPM / Vuln Management / Pure AppSec workflow improvements
1. [ ] Consider a more extensive blog post on migration from compliance pipelines

## Week of Oct 8, 2024

1. [ ] Update/clarify our plan in https://gitlab.com/gitlab-org/gitlab/-/issues/475152
1. [ ] Docs review - https://gitlab.com/gitlab-com/gitlab-OKRs/-/issues/8476 
1. [ ] Direction page updates
1. [ ] Customer discussion on compliance pipeline to pipeline execution policy migration
1. [ ] `90%` Sec Notifications Research Kickoff - https://gitlab.com/gitlab-org/ux-research/-/issues/2785
    - Candidate selection - https://gitlab.com/gitlab-org/ux-research/-/issues/3190 
    - Tested with 1 internal candidate, test with another
    - Identify more candidates to add to the study from interested customers
    - Create a structure for documenting findings and performing analysis
1. [ ] Check in on workflow status of all epics, ensure accuracy, line up next issues for planning breakdown and consider how to improve efficiencies here
    - Automation for tracking/updating in priority page
    - Identify necessary milestones to move those issues forward - video walkthroughs? Some checklist with engineering verification/signoff?
1. [ ] Get started with Verify on Opportunity Canvases -- Dependency Firewall, Deployement Approval, etc
    - Consider https://gitlab.com/gitlab-com/Product/-/issues/13528 
1. [ ] Org-level policy management - refine further and consider what is needed to make this more concrete
1. [ ] Put together a video walkthrough of https://gitlab.com/groups/gitlab-org/-/epics/10203, update designs listed in the epic

Next (not to be completed this week):

1. [ ] Consider a cross-functional roadmap with Compliance team
1. [ ] Finish out thoughts from strategy discussions in https://docs.google.com/document/d/1tQm6sRw3s7Am8yt8RQbSY5Ikhps2yZNj36UfOxhKftQ/edit 
1. [ ] Circle back on https://gitlab.com/gitlab-org/security-products/analyzers/container-scanning/-/merge_requests/3055#note_2079299303 
1. [ ] Check with a few customers on the license approval policy component filter plan
    - See if we have a way to view metrics for this type of policy
1. [ ] Research - policy priorities for customer calls - https://gitlab.com/gitlab-org/ux-research/-/issues/3084 
    - Questions for Organizations support
    - Excluding components
    - Additional security/compliance use cases research
1. [ ] Create/refine/find epic - OOTB policy templates (import, create from template, create from scratch)
    - Consider how this connects with compliance frameworks/requirements
1. [ ] Basic guidelines for contributing to policies - https://gitlab.com/gitlab-org/gitlab/-/issues/467345
1. [ ] `Started` Dogfooding: Propose guidance/DRIs for managing policies internally
1. [ ] `Started` Dogfooding: Reach back out to Darva/Sam to involve their stages

## Week of Oct 1, 2024
-- Note: OOO Saturday / traveling

1. [ ] Docs review - https://gitlab.com/gitlab-com/gitlab-OKRs/-/issues/8476 
1. [X] Review opportunity canvas for Dependency Firewall
1. [X] `90%` Create a better roadmap tracking issue/epic, with themes
    - Template to start [issue](https://gitlab.com/g.hickman/serenity/-/issues/21)
1. [ ] Direction page updates
1. [ ] Customer discussion on compliance pipeline to pipeline execution policy migration
1. [ ] `90%` Sec Notifications Research Kickoff - https://gitlab.com/gitlab-org/ux-research/-/issues/2785
    - Candidate selection - https://gitlab.com/gitlab-org/ux-research/-/issues/3190 
    - Tested with 1 internal candidate, test with another
    - Identify more candidates to add to the study from interested customers
    - Create a structure for documenting findings and performing analysis
1. [ ] Put together a video walkthrough of https://gitlab.com/groups/gitlab-org/-/epics/10203, update designs listed in the epic
1. [ ] Check in on workflow status of all epics, ensure accuracy, line up next issues for planning breakdown and consider how to improve efficiencies here
    - Automation for tracking/updating in priority page
    - Identify necessary milestones to move those issues forward - video walkthroughs? Some checklist with engineering verification/signoff?
1. [ ] Get started with Verify on Opportunity Canvases -- Dependency Firewall, Deployement Approval, etc
    - Consider https://gitlab.com/gitlab-com/Product/-/issues/13528 
1. [ ] Org-level policy management - refine further and consider what is needed to make this more concrete

Next (not to be completed this week):

1. [ ] Consider a cross-functional roadmap with Compliance team
1. [ ] Finish out thoughts from strategy discussions in https://docs.google.com/document/d/1tQm6sRw3s7Am8yt8RQbSY5Ikhps2yZNj36UfOxhKftQ/edit 
1. [ ] Circle back on https://gitlab.com/gitlab-org/security-products/analyzers/container-scanning/-/merge_requests/3055#note_2079299303 
1. [ ] Check with a few customers on the license approval policy component filter plan
    - See if we have a way to view metrics for this type of policy
1. [ ] Research - policy priorities for customer calls - https://gitlab.com/gitlab-org/ux-research/-/issues/3084 
    - Questions for Organizations support
    - Excluding components
    - Additional security/compliance use cases research
1. [ ] Create/refine/find epic - OOTB policy templates (import, create from template, create from scratch)
    - Consider how this connects with compliance frameworks/requirements
1. [ ] Basic guidelines for contributing to policies - https://gitlab.com/gitlab-org/gitlab/-/issues/467345
1. [ ] Dogfooding: Propose guidance/DRIs for managing policies internally
1. [ ] Dogfooding: Reach back out to Darva/Sam to involve their stages


## Week of Sep 24, 2024

1. [ ] Direction page updates
1. [ ] Check in on workflow status of all epics, ensure accuracy, line up next issues for planning breakdown and consider how to improve efficiencies here
    - Automation for tracking/updating in priority page
    - Identify necessary milestones to move those issues forward - video walkthroughs? Some checklist with engineering verification/signoff?
1. [ ] Customer discussion on compliance pipeline to pipeline execution policy migration
1. [ ] `90%` Sec Notifications Research Kickoff - https://gitlab.com/gitlab-org/ux-research/-/issues/2785
    - Candidate selection - https://gitlab.com/gitlab-org/ux-research/-/issues/3190 
    - Tested with 1 internal candidate, test with another
    - Identify more candidates to add to the study from interested customers
    - Create a structure for documenting findings and performing analysis
1. [ ] Put together a video walkthrough of https://gitlab.com/groups/gitlab-org/-/epics/10203, update designs listed in the epic
1. [ ] Create a better roadmap tracking issue/epic, with themes
    - Template to start [issue](https://gitlab.com/g.hickman/serenity/-/issues/21)
1. [X] Call set up this week to discuss allowlists/denylists across Sec - https://gitlab.com/gitlab-org/ux-research/-/issues/3196 
1. [ ] Get started with Verify on Opportunity Canvases -- Dependency Firewall, Deployement Approval, etc
    - Consider https://gitlab.com/gitlab-com/Product/-/issues/13528 
1. [ ] Org-level policy management - refine further and consider what is needed to make this more concrete

Next (not to be completed this week):

1. [ ] Finish out thoughts from strategy discussions in https://docs.google.com/document/d/1tQm6sRw3s7Am8yt8RQbSY5Ikhps2yZNj36UfOxhKftQ/edit 
1. [ ] Circle back on https://gitlab.com/gitlab-org/security-products/analyzers/container-scanning/-/merge_requests/3055#note_2079299303 
1. [ ] Check with a few customers on the license approval policy component filter plan
    - See if we have a way to view metrics for this type of policy
1. [ ] Research - policy priorities for customer calls - https://gitlab.com/gitlab-org/ux-research/-/issues/3084 
    - Questions for Organizations support
    - Excluding components
    - Additional security/compliance use cases research
1. [ ] Create/refine/find epic - OOTB policy templates (import, create from template, create from scratch)
    - Consider how this connects with compliance frameworks/requirements
1. [ ] Basic guidelines for contributing to policies - https://gitlab.com/gitlab-org/gitlab/-/issues/467345
1. [ ] Dogfooding: Propose guidance/DRIs for managing policies internally
1. [ ] Dogfooding: Reach back out to Darva/Sam to involve their stages

## Week of Sep 17, 2024

1. [ ] `90%` Sec Notifications Research Kickoff - https://gitlab.com/gitlab-org/ux-research/-/issues/2785
    - Candidate selection - https://gitlab.com/gitlab-org/ux-research/-/issues/3190 
    - Tested with internal candidate
    - Identify more candidates to add to the study from interested customers
    - Create a structure for documenting findings and performing analysis
1. [x] `95%` Exec level roadmap review for policies, with themes; create an [issue](https://gitlab.com/g.hickman/serenity/-/issues/21) to share as well
    - Consider two "speeds" of roadmaps
    - Polish and record

Next (not to be completed this week):

1. [ ] Call set up next week to discuss allowlists/denylists - https://gitlab.com/gitlab-org/ux-research/-/issues/3196 
1. [ ] Finish out thoughts from strategy discussions in https://docs.google.com/document/d/1tQm6sRw3s7Am8yt8RQbSY5Ikhps2yZNj36UfOxhKftQ/edit 
1. [ ] Circle back on https://gitlab.com/gitlab-org/security-products/analyzers/container-scanning/-/merge_requests/3055#note_2079299303 
1. [ ] Get started with Verify on Opportunity Canvases -- Dependency Firewall, Deployement Approval, etc
    - Consider https://gitlab.com/gitlab-com/Product/-/issues/13528 
1. [ ] Check with a few customers on the license approval policy component filter plan
    - See if we have a way to view metrics for this type of policy
1. [ ] Research - policy priorities for customer calls - https://gitlab.com/gitlab-org/ux-research/-/issues/3084 
    - Questions for Organizations support
    - Excluding components
    - Additional security/compliance use cases research
1. [ ] Create/refine/find epic - OOTB policy templates (import, create from template, create from scratch)
    - Consider how this connects with compliance frameworks/requirements
1. [ ] Basic guidelines for contributing to policies - https://gitlab.com/gitlab-org/gitlab/-/issues/467345
1. [ ] Dogfooding: Propose guidance/DRIs for managing policies internally
1. [ ] Dogfooding: Reach back out to Darva/Sam to involve their stages


## Week of Sep 11, 2024

Note: FF Day Sat, Sept 14

1. [ ] `80%` Sec Notifications Research Kickoff - https://gitlab.com/gitlab-org/ux-research/-/issues/2785
    - Polish the exercise and prep templates 
    - Candidate selection - https://gitlab.com/gitlab-org/ux-research/-/issues/3190 
1. [ ] `75%` Exec level roadmap review for policies, with themes; create an [issue](https://gitlab.com/g.hickman/serenity/-/issues/21) to share as well
    - Consider two "speeds" of roadmaps
1. [X] Customer escalation
1. [X] Sec assurance dogfooding prep

Next (not to be completed this week):
1. [X] Finalize UX research / discussion guide / excercises - https://gitlab.com/gitlab-org/ux-research/-/issues/2785#note_2079912208
1. [ ] Finish out thoughts from strategy discussions in https://docs.google.com/document/d/1tQm6sRw3s7Am8yt8RQbSY5Ikhps2yZNj36UfOxhKftQ/edit 
1. [X] Ian's blog post - post migration flow - check in with Nate Tuesday Sept 10
1. [ ] Circle back on https://gitlab.com/gitlab-org/security-products/analyzers/container-scanning/-/merge_requests/3055#note_2079299303 
1. [ ] Get started with Verify on Opportunity Canvases -- Dependency Firewall, Deployement Approval, etc
    - Consider https://gitlab.com/gitlab-com/Product/-/issues/13528 
1. [ ] Check with a few customers on the license approval policy component filter plan
    - See if we have a way to view metrics for this type of policy
1. [ ] Research - policy priorities for customer calls - https://gitlab.com/gitlab-org/ux-research/-/issues/3084 
    - Questions for Organizations support
    - Excluding components
    - Additional security/compliance use cases research
1. [ ] Create/refine/find epic - OOTB policy templates (import, create from template, create from scratch)
1. [ ] Basic guidelines for contributing to policies - https://gitlab.com/gitlab-org/gitlab/-/issues/467345
1. [ ] Dogfooding: Propose guidance/DRIs for managing policies internally
1. [ ] Dogfooding: Reach back out to Darva/Sam to involve their stages
1. [ ] Schedule a retro on PEP with Verify


## Week of Aug 27, 2024

Note: OOO Thursday Aug 19 - Sep 3

High Priority:
1. [X] Prep / Attend Govern Strategy Session 1
1. [X] Any thoughts on categories
1. [X] Need to provide design updates for how to handle scheduled pipeline execution
1. [ ] `40%` Sec Notifications Research - https://gitlab.com/gitlab-org/ux-research/-/issues/2785
    - Aligning with Karen on research/interview questions for a qual
    - Unblocked, moving forward as PDI data is limited
    - Potential still do explore more in parallel
1. [ ] `75%` Exec level roadmap review for policies, with themes; create an [issue](https://gitlab.com/g.hickman/serenity/-/issues/21) to share as well
    - Consider two "speeds" of roadmaps


Next (not to be completed this week):
1. [ ] Finalize UX research / discussion guide / excercises - https://gitlab.com/gitlab-org/ux-research/-/issues/2785#note_2079912208
1. [ ] Finish out thoughts from strategy discussions in https://docs.google.com/document/d/1tQm6sRw3s7Am8yt8RQbSY5Ikhps2yZNj36UfOxhKftQ/edit 
1. [ ] Ian's blog post - post migration flow - check in with Nate Tuesday Sept 10
1. [ ] Circle back on https://gitlab.com/gitlab-org/security-products/analyzers/container-scanning/-/merge_requests/3055#note_2079299303 
1. [ ] Get started with Verify on Opportunity Canvases -- Dependency Firewall, Deployement Approval, etc
    - Consider https://gitlab.com/gitlab-com/Product/-/issues/13528 
1. [ ] Check with a few customers on the license approval policy component filter plan
    - See if we have a way to view metrics for this type of policy
1. [ ] Research - policy priorities for customer calls - https://gitlab.com/gitlab-org/ux-research/-/issues/3084 
    - Questions for Organizations support
    - Excluding components
    - Additional security/compliance use cases research
1. [ ] Create/refine/find epic - OOTB policy templates (import, create from template, create from scratch)
1. [ ] Basic guidelines for contributing to policies - https://gitlab.com/gitlab-org/gitlab/-/issues/467345
1. [ ] Dogfooding: Propose guidance/DRIs for managing policies internally
1. [ ] Dogfooding: Reach back out to Darva/Sam to involve their stages
1. [ ] Schedule a retro on PEP with Verify


## Week of Aug 20, 2024

High Priority:
1. [ ] By Friday, any thoughts on categories
1. [X] Prep / Attend Govern Strategy Session 1
1. [ ] Need to provide design updates for how to handle scheduled pipeline execution
1. [ ] `75%` Exec level roadmap review for policies, with themes; create an [issue](https://gitlab.com/g.hickman/serenity/-/issues/21) to share as well
    - Consider two "speeds" of roadmaps
1. [X] `100%` Prep/refine any immediate improvements planned for PEP -- removing from my list as this one seems on track
    - Handling needs statements, esp for pre stage
    - Handling job name conflicts gracefully (any approach better than using an index?)
1. [ ] `30% - Blocked` Sec Notifications Research - https://gitlab.com/gitlab-org/ux-research/-/issues/2785
    - Working with data team to identify broken workflows
    - Blocked, not much data from PDI
1. [X] `99%` Dogfooding: Finish moving License Approval Policy to global level - 🎉

Medium Priority:
1. [ ] Check with a few customers on the license approval policy component filter plan
    - See if we have a way to view metrics for this type of policy
1. [ ] Research - policy priorities for customer calls - https://gitlab.com/gitlab-org/ux-research/-/issues/3084 
    - Questions for Organizations support
    - Excluding components
    - Additional security/compliance use cases research
1. [ ] Create/refine/find epic - OOTB policy templates (import, create from template, create from scratch)
1. [ ] Basic guidelines for contributing to policies - https://gitlab.com/gitlab-org/gitlab/-/issues/467345
1. [ ] Dogfooding: Propose guidance/DRIs for managing policies internally
1. [ ] Dogfooding: Reach back out to Darva/Sam to involve their stages
1. [ ] Get started with Verify on Opportunity Canvases -- Dependency Firewall, Deployement Approval, etc
1. [ ] Schedule a retro on PEP with Verify

Next:



## Week of Aug 13, 2024

High Priority:
1. [X] Govern + Verify call (met with Jackie but also moved the broader meeting again)
1. [ ] Need to provide design updates for how to handle scheduled pipeline execution
1. [ ] `75%` Exec level roadmap review for policies, with themes; create an [issue](https://gitlab.com/g.hickman/serenity/-/issues/21) to share as well
1. [ ] `50%` Prep/refine any immediate improvements planned for PEP
    - Handling needs statements, esp for pre stage
    - Handling job name conflicts gracefully (any approach better than using an index?)
1. [X] Pipeline execution policies group retro
1. [ ] `30% - Blocked` Sec Notifications Research - https://gitlab.com/gitlab-org/ux-research/-/issues/2785
    - Working with data team to identify broken workflows
    - Blocked, not much data from PDI
1. [X] Create pen test issue
1. [ ] `99%` Dogfooding: Finish moving License Approval Policy to global level

Medium Priority:
1. [ ] Check with a few customers on the license approval policy component filter plan
    - See if we have a way to view metrics for this type of policy
1. [ ] Research - policy priorities for customer calls - https://gitlab.com/gitlab-org/ux-research/-/issues/3084 
    - Questions for Organizations support
    - Excluding components
    - Additional security/compliance use cases research
1. [ ] Create/refine/find epic - OOTB policy templates (import, create from template, create from scratch)
1. [ ] Basic guidelines for contributing to policies - https://gitlab.com/gitlab-org/gitlab/-/issues/467345
1. [ ] Dogfooding: Propose guidance/DRIs for managing policies internally
1. [ ] Dogfooding: Reach back out to Darva/Sam to involve their stages
1. [ ] Get started with Verify on Opportunity Canvases -- Dependency Firewall, Deployement Approval, etc

Next:
1. [ ] Schedule a retro on PEP with Verify

## Week of Aug 06, 2024

High Priority:
1. [X] Sec Section Dogfooding sync
1. [ ] `25%` Exec level roadmap review for policies, with themes; create an [issue](https://gitlab.com/g.hickman/serenity/-/issues/21) to share as well
1. [ ] `Blocked` Prep/refine any immediate improvements planned for PEP
    - Handling needs statements, esp for pre stage
    - Handling job name conflicts gracefully (any approach better than using an index?)
    - Blocked by Verify
1. [ ] `25%` Sec Notifications Research - https://gitlab.com/gitlab-org/ux-research/-/issues/2785
1. [ ] Check with a few customers on the license approval policy component filter plan
    - See if we have a way to view metrics for this type of policy
1. [ ] Research - policy priorities for customer calls - https://gitlab.com/gitlab-org/ux-research/-/issues/3084 
    - Questions for Organizations support
    - Excluding components
    - Additional security/compliance use cases research
1. [ ] Govern + Verify call (met with Jackie but also moved the broader meeting again)
1. [ ] Create pen test issue


Medium Priority:
1. [X] Create/refine epic - Export policy history - https://gitlab.com/gitlab-org/gitlab/-/issues/474249 
1. [ ] Create/refine/find epic - OOTB policy templates (import, create from template, create from scratch)
1. [ ] Basic guidelines for contributing to policies - https://gitlab.com/gitlab-org/gitlab/-/issues/467345
1. [ ] `99%` Dogfooding: Finish moving License Approval Policy to global level, 
1. [ ] Dogfooding: Propose guidance/DRIs for managing policies internally
1. [ ] Dogfooding: Reach back out to Darva/Sam to involve their stages
1. [ ] Get started with Verify on Opportunity Canvases -- Dependency Firewall, Deployement Approval, etc

## Week of Jul 30, 2024

High Priority:
1. [ ] `25%` Exec level roadmap review for policies, with themes
1. [ ] `Started` Prep/refine any immediate improvements planned for PEP
    - Handling needs statements, esp for pre stage
    - Handling job name conflicts gracefully (any approach better than using an index?)
1. [ ] `25%` Sec Notifications Research - https://gitlab.com/gitlab-org/ux-research/-/issues/2785
1. [ ] Check with a few customers on the license approval policy component filter plan
1. [ ] Research - policy priorities for customer calls - https://gitlab.com/gitlab-org/ux-research/-/issues/3084 
    - Questions for Organizations support
    - Excluding components
    - Additional security/compliance use cases research
1. [-] Govern + Verify call (moved to next week)
1. [X] Sec PM meeting prep
1. [X] [Compliance Pipeline Comms Prep](https://docs.google.com/presentation/d/1MJs-_QuoaVmP4weGYaMRww1xabS05XtMdgpk6kx8xlg/edit#slide=id.g279caf4350a_0_15)
1. [ ] Create/refine epic - Export policy history - https://gitlab.com/gitlab-org/gitlab/-/issues/474249 
1. [ ] Create/refine/find epic - OOTB policy templates (import, create from template, create from scratch)
1. [X] [Design priorities](https://gitlab.com/gitlab-org/gitlab/-/issues/473846)
1. [X] Prep for Tuesday customer call
1. [X] Release post prep

Medium Priority:
1. [ ] Prep for calls ~2 next week
1. [ ] Basic guidelines for contributing to policies - https://gitlab.com/gitlab-org/gitlab/-/issues/467345
1. [ ] `90%` Dogfooding: Finish moving License Approval Policy to global level, 
1. [ ] Dogfooding: Propose guidance/DRIs for managing policies internally
1. [ ] Dogfooding: Reach back out to Darva/Sam to involve their stages


## Week of Jul 23, 2024

OOO Wednesday

High Priority:
1. [ ] `Started` Exec level roadmap review for policies, with themes
1. [ ] `Started` Prep/refine any immediate improvements planned for PEP
    - Handling needs statements, esp for pre stage
    - Handling job name conflicts gracefully (any approach better than using an index?)
1. [ ] `Started` Sec Notifications Research - https://gitlab.com/gitlab-org/ux-research/-/issues/2785
1. [ ] Check with a few customers on the license approval policy component filter plan
1. [X] Prep for roadmap call next Tuesday
1. [X] Security Vision Use Case - write mission statement for Thursday

Medium Priority:
1. [ ] Basic guidelines for contributing to policies - https://gitlab.com/gitlab-org/gitlab/-/issues/467345
1. [ ] Research - policy priorities for customer calls - https://gitlab.com/gitlab-org/ux-research/-/issues/3084 
1. [ ] Finish moving License Approval Policy to global level, propose guidance/DRIs for managing this, reach back out to Darva/Sam to involve their stages


## Week of Jul 16, 2024

High Priority:
1. [ ] `Started` Exec level roadmap review for policies, with themes
1. [X] Prep a Pipeline Execution Policy demo
1. [ ] `Started` Prep/refine any immediate improvements planned for PEP
    - Handling needs statements, esp for pre stage
    - Handling job name conflicts gracefully (any approach better than using an index?)
1. [ ] `Started` Sec Notifications Research - https://gitlab.com/gitlab-org/ux-research/-/issues/2785
1. [ ] Check with a few customers on the license approval policy component filter plan
1. [ ] Finish moving License Approval Policy to global level, propose guidance/DRIs for managing this, reach back out to Darva/Sam to involve their stages

Medium Priority:
1. [ ] Basic guidelines for contributing to policies - https://gitlab.com/gitlab-org/gitlab/-/issues/467345
1. [ ] Research - policy priorities for customer calls - https://gitlab.com/gitlab-org/ux-research/-/issues/3084 

## Week of Jul 09, 2024

High Priority:
1. [ ] `Started` Exec level roadmap review for policies, with themes
1. [ ] Prep/refine any immediate improvements planned for PEP
1. [ ] Sec Notifications Research - https://gitlab.com/gitlab-org/ux-research/-/issues/2785
1. [ ] Check with a few customers on the license approval policy component filter plan
1. [ ] Finish moving License Approval Policy to global level, propose guidance/DRIs for managing this, reach back out to Darva/Sam to involve their stages

Medium Priority:
1. [ ] Basic guidelines for contributing to policies - https://gitlab.com/gitlab-org/gitlab/-/issues/467345
1. [ ] Research - policy priorities for customer calls - https://gitlab.com/gitlab-org/ux-research/-/issues/3084 


## Week of Jul 02, 2024

** Week of July 4th holiday -- working July 4th, off 5th and 6th

1. [X] Prep for next dogfooding session, next steps on centralizing policies
    - How to get policies to a global level
1. [ ] Check with a few customers on the license approval policy component filter plan
1. [ ] `Started` Exec level roadmap review for policies, with themes
1. [X] Ensure that pipeline execution policies are fully ready to release successfully
1. [ ] Prep/refine any immediate improvements planned for PEP
1. [ ] Basic guidelines for contributing to policies - https://gitlab.com/gitlab-org/gitlab/-/issues/467345
1. [ ] Research - policy priorities for customer calls - https://gitlab.com/gitlab-org/ux-research/-/issues/3084 
1. [ ] Sec Notifications Research - https://gitlab.com/gitlab-org/ux-research/-/issues/2785


## Week of Jun 25, 2024

1. [ ] Basic guidelines for contributing to policies - https://gitlab.com/gitlab-org/gitlab/-/issues/467345
1. [ ] Research plan for customer calls - https://gitlab.com/gitlab-org/ux-research/-/issues/3084 
1. [ ] Sec Notifications Research - https://gitlab.com/gitlab-org/ux-research/-/issues/2785
1. [ ] Prep for next dogfooding session, next steps on centralizing policies


## Week of Jun 11, 2024

Note: Working Friday (F&F day), but observing this on Saturday instead (4 day week)

1. [ ] Do a short walkthrough of the recent updates for pipeline execution policy, prep Documentation
    - https://gitlab.com/groups/gitlab-org/-/epics/13266
    - https://gitlab.com/gitlab-org/govern/security-policies/martins-test-group/pipeline-execution-policies
    - https://www.youtube.com/watch?v=y0u588UKEzk 
    - https://docs.google.com/document/d/1afDyag9Aatl-6gsb1NQ7h9_8n6JHQGOmQEIOmLGwLuI/edit#heading=h.d284upfq2hc0 
1. [ ] Refine the follow-up improvements for PEP and add to direction/priorities
1. [ ] Clarity and prep around compliance pipeline deprecation
1. [ ] Get a better structure for keeping up with epics within each stage of workflow, and get 2-3 epics/issues into refinement
    - Again, need some way to track interest in each feature more easily
    - Set up a list of questions I want answered from customers - e.g. priority of policy types, org level management/access of policies and frameworks, validation of handling vuln exceptions/allowlist
    - Quick way to attribute customers to each feature with some data - licenses, interest level, revenue, adoption impact.. etc
1. [ ] Some customer validation around which policy types are most critical would be of use - e.g. push rules? deployment approvals? external status checks? others? This would help us understand better for security and compliance users which areas of the DevSecOps lifecycle are most critical to them right now and would direct our prioritization.
1. [ ] Prep UX Research for security notifications
1. [ ] Consistent way to handle analyzer scenarios in policies - e.g. DS behavior if over 10k files exist
1. [ ] DAST policy demo - started on this, created example policy
1. [ ] Escalation Support - Sam taking lead but I will follow along and answer any questions I can
    - https://gitlab.com/gitlab-org/gitlab/-/issues/462890
    - https://gitlab.com/gitlab-org/gitlab/-/issues/463603
1. [ ] Follow up to share resources from a prev customer call, examples of how to get data back into GitLab in pipeline, e.g. test samples, anything else of use here - https://gitlab.dovetail.com/data/KfW-2iuMBRVVJf5P2DT2U1JAY0
1. [ ] Work on better data for policies and compliance - ability to understand usage by customer, can we also gauge satisfaction?
1. [ ] Check in on RP items and make sure they are good to go
1. [ ] Help Alan on planning issue - https://gitlab.com/gitlab-org/gitlab/-/issues/466291 
1. [ ] OKR Tracking - https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/6981

## Week of 2024-06-04

1. [ ] Escalation Support
    - https://gitlab.com/gitlab-org/gitlab/-/issues/462890
    - https://gitlab.com/gitlab-org/gitlab/-/issues/463603
1. [X] 3 customer calls / call prep
    - https://gitlab.dovetail.com/data/4x78Ol2N7vf3tGqQffRGFw
    - https://gitlab.dovetail.com/data/6AJt0QHw4xn0Oit96OtUSQ
    - https://gitlab.dovetail.com/data/6eABeL1n0iHiSpWRp0JYaP 
    - Sec Section Dogfooding - https://docs.google.com/document/d/1eFlywvCeQXyDduuDueNIt1oz7IVGw7P3KKTCCYxAcWA/edit#heading=h.7rfpw0x53uo1
1. [ ] Consistent way to handle analyzer scenarios in policies - e.g. DS behavior if over 10k files exist
1. [ ] DAST policy demo - started on this, created example policy
1. [X] More examples of happy customers, or a system to get this data
1. [ ] Follow up to share resources from a prev customer call, examples of how to get data back into GitLab in pipeline, e.g. test samples, anything else of use here - https://gitlab.dovetail.com/data/KfW-2iuMBRVVJf5P2DT2U1JAY0
1. [X] For next Sec assurance meeting, share more examples of checks/violations and roadmap plan here, any other follow-ups from call Tues, update sync to include Cynthia optionally
1. [ ] Some customer validation around which policy types are most critical would be of use - e.g. push rules? deployment approvals? external status checks? others? This would help us understand better for security and compliance users which areas of the DevSecOps lifecycle are most critical to them right now and would direct our prioritization.
1. [X] Finish creating/prepping release posts
    - https://gitlab.com/gitlab-org/gitlab/-/issues/461021
    - https://gitlab.com/dashboard/merge_requests?assignee_username=g.hickman 
1. [ ] Get a better structure for keeping up with epics within each stage of workflow, and get 2-3 epics/issues into refinement
    - Again, need some way to track interest in each feature more easily
    - Set up a list of questions I want answered from customers - e.g. priority of policy types, org level management/access of policies and frameworks, validation of handling vuln exceptions/allowlist
    - Quick way to attribute customers to each feature with some data - licenses, interest level, revenue, adoption impact.. etc

Recurring Meetings:

- Security Assurance Dogfooding - Monthly
- Sec + AppSec & Legal Dogfooding - Monthly
- Govern + Verify - Monthly
- Nate + Ian - Weekly
- Sam - Weekly
- Govern Policies - Weekly
- Camellia - Biweekly
- Tim / Grant Mentorship - Biweekly
- Pipeline Execution Policy - Weekly
- Justin - Monthly
- Sec PM Teem Meeting - Biweekly
- Joe Longo - Monthly



## Week of 2024-05-28
1. [X] Finish Update Direction pages for Security Policies -- updates for status check policy type, clearer priority on hide SPP plans, handling various approver scenarios
1. [ ] Finish Prep MRs for release post items in advance of 17.1 and 17.2 - get wayyy ahead!
1. [ ] Get a better structure for keeping up with epics within each stage of workflow, and get 2-3 epics/issues into refinement
1. [X] License approval dogfooding call -- update cadence, see if we can restructure the call to include AppSec
1. [ ] Validation of impact on YAML / UX to adoption of security and compliance features -- finish creating a survey on this
1. [ ] DAST policy demo - started on this, created example policy
1. [ ] Follow up to share resources from last Wed morning call to customer, examples of how to get data back into GitLab in pipeline, e.g. test samples, anything else of use here
1. [ ] For next Sec assurance meeting, share more examples of checks/violations and roadmap plan here, any other follow-ups from call Tues, update sync to include Cynthia optionally
1. [X] Establish a monthly Govern + AppSec sync call, possibly merge in License Approval Dogfooding cadence
1. [ ] Some customer validation around which policy types are most critical would be of use - e.g. push rules? deployment approvals? external status checks? others? This would help us understand better for security and compliance users which areas of the DevSecOps lifecycle are most critical to them right now and would direct our prioritization.
1. [ ] Generate release posts
    - https://gitlab.com/gitlab-org/gitlab/-/issues/461021
    - https://gitlab.com/dashboard/merge_requests?assignee_username=g.hickman 


## Week of 2024-05-21
1. [ ] Update Direction pages for Security Policies -- include fix for MR settings on all policies, updates for status check policy type
1. [ ] Prep MRs for release post items in advance of 17.1 and 17.2 - get wayyy ahead!
1. [ ] Get a better structure for keeping up with epics within each stage of workflow, and get 2-3 epics/issues into refinement
1. [X] Go through all feedback on pipeline execution yaml and prep for Wednesday call
1. [x] Prep for customer calls this week
1. [X] Govern stage strategy call - prep and recording
1. [ ] License approval dogfooding call -- update cadence, see if we can restructure the call to include AppSec
1. [ ] Validation of impact on YAML / UX to adoption of security and compliance features -- finish creating a survey on this
1. [ ] DAST policy demo - started on this, created example policy
1. [ ] Follow up to share resources from last Wed morning call to customer, examples of how to get data back into GitLab in pipeline, e.g. test samples, anything else of use here
1. [ ] For next Sec assurance meeting, share more examples of checks/violations and roadmap plan here, any other follow-ups from call Tues, update sync to include Cynthia optionally
1. [ ] Establish a monthly Govern + AppSec sync call, possibly merge in License Approval Dogfooding cadence
1. [ ] Some customer validation around which policy types are most critical would be of use - e.g. push rules? deployment approvals? external status checks? others? This would help us understand better for security and compliance users which areas of the DevSecOps lifecycle are most critical to them right now and would direct our prioritization.
1. [ ] Generate release posts
    - https://gitlab.com/gitlab-org/gitlab/-/issues/461021
    - https://gitlab.com/dashboard/merge_requests?assignee_username=g.hickman 


Backlog/Follow-up questions:
1. [ ] Can you cancel an SEP job?
1. [ ] Does the job run differently if you rerun it in the job/pipeline screen?
1. [ ] Create a few Enablement bites for Compliance - work with Ian on this?
1. [ ] Development guide

Optional questions to reflect on for planning:
1. What are 2-3 things that are highest impact?
    - Any efforts that help successful launch of PEP
    - Clearer understanding of what's next for policies and why those priorities (e.g. better linking of customer need to the planned items, better context in issues)
    - Better comms of planning: Updating the epics, issues, direction pages, and prepping MRs so plans are communicated there, then can work on adding videos like enablement bites, feature proposal walkthroughs, or kickoff videos
    - Refine and share out plans for hiding security policies / organization support
1. What are 2-3 things I can say no to?
    - Maybe say no to having so many customer meetings that are sales focused, but can trade this for more customer validation/discovery
    - No to sales enablement materials right now -- use what we have for now
    - Instead of working on compliance enablement bites consider delegating
1. Who do I need to collaborate with or seek input from?
1. What did I accomplish last week, and what can be improved?
1. Are there any dependencies I need to address?


## Week of 2024-05-14
1. [X] Review compliance roadmap/plan by EOW
1. [X] License Approval Policies next steps - get with Dev and Ops to identify contacts, work on this: https://gitlab.com/gitlab-com/legal-and-compliance/-/issues/1364#note_1896925640 -- updated approvers, fixed some policy issues, reached out to potential DRIs for Dev and Ops via the issue
1. [X] Update docs: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/150067 
1. [ ] Can you cancel an SEP job?
1. [ ] Does the job run differently if you rerun it in the job/pipeline screen?
1. [ ] Create a few Enablement bites for Compliance - work with Ian on this?
1. [ ] Validation of impact on YAML / UX to adoption of security and compliance features -- finish creating a survey on this
1. [X] Validation of performance/maintenance implications of YAML backed compliance management
1. [ ] DAST policy demo - started on this, created example policy

Follow-ups captured during the week
1. [ ] Resources for Wed morning call to customer, examples of how to get data back into GitLab in pipeline, e.g. test samples, anything else of use here
1. [ ] For next Sec assurance meeting, share more examples of checks/violations and roadmap plan here, any other follow-ups from call Tues, update sync to include Cynthia optionally
1. [ ] Establish a monthly Govern + AppSec sync call, possibly merge in License Approval Dogfooding cadence


## Week of 2024-05-07

Out on vacation

## Week of 2024-04-23 (My schedule is Tues-Sat)

1. [X] Customer calls
    - 2 Tuesday
    - 1 Surprise Wednesday call
    - 2 Thursday - [One](https://gitlab.dovetail.com/data/NCBI-5r9Dhd2J4xMg7aontEJjmy) and [Two](https://gitlab.dovetail.com/data/T-Mobile-7334JvEuDNLjxz6TwoS2XS)
    - 1 Friday call but seems to be getting rescheduled
1. [X] License Approval Policies next steps
1. [X] Set up coverage issue
1. [X] Clarify Compliance Management plans with leadership
1. [ ] Validation of impact on YAML / UX to adoption of security and compliance features
1. [ ] Validation of performance/maintenance implications of YAML backed compliance management
1. [ ] ~SF partnership/integration next steps - potential to get support from Ian on this one, Sam helping with partner aspects~ handoff to Ian
    - Need more dev accounts
    - Partner paperwork and try to get accounts this way
1. [X] Set up a regular Verify + Govern sync post-Summit
1. [ ] Create a few Enablement bites for Compliance
1. [ ] Update docs: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/150067 
1. [ ] DAST policy demo

Backlog:

1. [ ] License Approval Policies next steps - see how to get policies enabled at group level with scopes, try to tee up DRIs in Dev and Ops.
1. [ ] Can you cancel an SEP job?
1. [ ] Does the job run differently if you rerun it in the job/pipeline screen?



## Week of 2024-04-16

1. [ ] Clarify Compliance Management plans with leadership
1. [ ] Validation of impact on YAML / UX to adoption of security and compliance features
1. [ ] Validation of performance/maintenance implications of YAML backed compliance management
1. [X] Release post for policy bot comments
1. [ ] Close out https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/6050 - plans for compliance management, update Direction page, and update https://gitlab.com/groups/gitlab-org/-/epics/720. 
1. [ ] Create a few Enablement bites for Compliance
1. [ ] SF partnership/integration next steps
    - Need more dev accounts
    - Partner paperwork and try to get accounts this way
1. [X] Reassess pipeline execution policy next steps - working now with Martin and he seems to have a handle on it

## Week of 2024-04-09

Out sick

## Week of 2024-04-03

1. [ ] Clarify Compliance Management plans with leadership
1. [ ] Close out https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/6050 - plans for compliance management, update Direction page, and update https://gitlab.com/groups/gitlab-org/-/epics/720. 
1. [ ] Create a few Enablement bites for Compliance
1. [ ] SF partnership/integration next steps
    - Need more dev accounts
    - Partner paperwork and try to get accounts this way
1. [ ] Set up a regular Verify + Govern sync post-Summit
1. [ ] Somehow get agreement on 17.0 reqs for pipeline execution policy this week


## Week of 2024-03-26 

1. [ ] Shadow Sam W. and watch a few of the recordings from him 
1. [X] Threat insights cross-train
1. [X] Get UX design plans tighter
1. [X] Prep for at least 3 customer calls this week
1. [ ] Close out https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/6050 - plans for compliance management, update Direction page, and update https://gitlab.com/groups/gitlab-org/-/epics/720. 
1. [ ] Create a few Enablement bites for Compliance
1. [ ] SF partnership/integration next steps
    - Need more dev accounts
    - Partner paperwork and try to get accounts this way
1. [ ] Set up a regular Verify + Govern sync post-Summit
1. [ ] Somehow get agreement on 17.0 reqs for pipeline execution policy this week

Backlog:

1. [ ] https://about.gitlab.com/services/education/gitlab-security-specialist/
1. [ ] https://docs.gitlab.com/ee/administration/broadcast_messages.html#add-a-broadcast-message - Useful for quicker generic messaging in our product feature pages?


## Week of 2024-03-20 

Note: Started on Wednesday following Summit.

1. [ ] Close out https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/6050 - plans for compliance management, update Direction page, and update https://gitlab.com/groups/gitlab-org/-/epics/720. 
1. [X] Share out [Compliance Management Direction](https://www.youtube.com/watch?v=5Tv4RGj-msw) more broadly
1. [ ] Create a few Enablement bites for Compliance
1. [ ] SF partnership/integration next steps
    - Need more dev accounts
    - Partner paperwork and try to get accounts this way
1. [ ] Set up a regular Verify + Govern sync post-Summit

## Week of 2024-03-11 (GitLab Summit, starting Monday)

With the Summit, not many objectives to complete work, but will track to-dos for after Summit and takeways from Summit.

Takeaways:

Backlog:
1. [ ] Sync on [Registry Policies](https://gitlab.com/groups/gitlab-org/-/epics/5133)
1. [ ] License Compliance dogfooding (see [doc](https://docs.google.com/document/d/12HPySY8GmzIM6B_arq1K5L6sZXyoZFoVVxv6SJv8AZA/edit))
1. [ ] SF partnership/integration next steps
    - Need more dev accounts
    - Partner paperwork and try to get accounts this way
1. [ ] Follow up with Jeff to wipe old laptop
1. [ ] Set up a regular Verify + Govern sync post-Summit
1. [ ] Quick walkthrough clip for https://gitlab.com/groups/gitlab-org/-/epics/8084?
1. [ ] Refine notifications UX research
1. [ ] Create a script for testing MR changes?
1. [ ] Watch some CI training videos on various advanced features. (Pick up [here](https://levelup.gitlab.com/learn/course/gitlab-runners/gitlab-runners/getting-started-with-gitlab-runners?client=internal-team-members&page=4))
1. [ ] Set up more workflows for ruthless prioritization and blocking out distractions, find opportunities for delegation
1. [ ] Create a dashboard or tool to track workflow labels and make sure it's easier to track state/progress
1. [ ] Blog post for pipeline execution / policy scoping
1. [ ] Refine https://gitlab.com/gitlab-org/gitlab/-/issues/444459, [Figjam](https://www.figma.com/file/cHWowVN1ouNYjwzDj658vZ/Policies-should-be-evaluated-as-a-merge-check?type=whiteboard&node-id=0-1&t=offtnZ2pER2YbGOG-0)

## Week of 2024-03-05

1. [X] Get with folks on License Compliance dogfooding (see [doc](https://docs.google.com/document/d/12HPySY8GmzIM6B_arq1K5L6sZXyoZFoVVxv6SJv8AZA/edit))
1. [X] Finish SAFE training before Summit
1. [ ] Compliance enablement bites
    - Discuss with Nate what might be useful here for Compliance
    - Overview on where we're heading - checks library, SOC 2 checks, custom checks with policies
1. [X] Interpet Compliance survey data - [spreadsheet](https://docs.google.com/spreadsheets/d/1Ys95tAAAToRcyrdtFNHlHsc3OhpmdoywzlMKVIuPEU0/edit#gid=1641480119)
1. [ ] Identify a few compliance focused customers from the survey (or elsewhere) and begin a pipeline of customer calls, define a set of key questions to get answered
    - e.g. validate if they absolutely require Org-level management, multiple CF labels
    - Do the top results from survey align with top Ultimate customer needs as well?
1. [X] Refine https://gitlab.com/gitlab-org/gitlab/-/issues/441095
1. [ ] Any further prep required for Summit? 
    - Dry cleaners? Outfits?
    - Pre-packing?
    - Activities?
    - Any types of gifts?
1. [ ] SF partnership/integration next steps
    - Need more dev accounts
    - Partner paperwork and try to get accounts this way
1. [X] Test CI component in Pipeline Execution Policy Action
1. [X] Breakdown for Cells work - Security Policies - https://gitlab.com/gitlab-org/gitlab/-/issues/434993 - Confirm high level estimation
1. [X] Breakdown for Cells work - Compliance - https://gitlab.com/gitlab-org/gitlab/-/issues/434995 - Confirm high level estimation
1. [ ] Follow up with Jeff to wipe old laptop
1. [ ] Set up a regular Verify + Govern sync post-Summit
1. [ ] Quick walkthrough clip for https://gitlab.com/groups/gitlab-org/-/epics/8084?

Backlog:

1. [ ] Refine notifications UX research
1. [ ] Create a script for testing MR changes?
1. [ ] Watch some CI training videos on various advanced features. (Pick up [here](https://levelup.gitlab.com/learn/course/gitlab-runners/gitlab-runners/getting-started-with-gitlab-runners?client=internal-team-members&page=4))
1. [ ] Set up more workflows for ruthless prioritization and blocking out distractions, find opportunities for delegation
1. [ ] Create a dashboard or tool to track workflow labels and make sure it's easier to track state/progress
1. [ ] Blog post for pipeline execution / policy scoping
1. [ ] Refine https://gitlab.com/gitlab-org/gitlab/-/issues/444459, [Figjam](https://www.figma.com/file/cHWowVN1ouNYjwzDj658vZ/Policies-should-be-evaluated-as-a-merge-check?type=whiteboard&node-id=0-1&t=offtnZ2pER2YbGOG-0)


## Week of 2024-02-29 (start on Thursday after PTO)

Note: Returning from PTO 02-29

1. [ ] Get with folks on License Compliance dogfooding (see [doc](https://docs.google.com/document/d/12HPySY8GmzIM6B_arq1K5L6sZXyoZFoVVxv6SJv8AZA/edit))
1. [ ] Compliance enablement bites
    - Discuss with Nate what might be useful here for Compliance
    - Overview on where we're heading - checks library, SOC 2 checks, custom checks with policies
1. [ ] Interpet Compliance survey data - [spreadsheet](https://docs.google.com/spreadsheets/d/1Ys95tAAAToRcyrdtFNHlHsc3OhpmdoywzlMKVIuPEU0/edit#gid=1641480119)
1. [ ] Identify a few compliance focused customers from the survey (or elsewhere) and begin a pipeline of customer calls, define a set of key questions to get answered
    - e.g. validate if they absolutely require Org-level management, multiple CF labels
    - Do the top results from survey align with top Ultimate customer needs as well?
1. [ ] Refine https://gitlab.com/gitlab-org/gitlab/-/issues/441095
1. [ ] Any further prep required for Summit? 
    - Dry cleaners? Outfits?
    - Pre-packing?
    - Activities?
    - Any types of gifts?
1. [ ] SF partnership/integration next steps
    - Need more dev accounts
    - Partner paperwork and try to get accounts this way

## Week of 2024-02-06 (week starting on Tuesday)

Note: PTO through O2-21 to 02-28

Priorities:

1. [X] Get Sam slides on compliance vision
1. [ ] 4-5 customer calls are taking lots of my time/priority
1. [ ] Doublecheck on MRs for 16.9 (SP & Compliance) - opp for more automation?? -- Should have these done by EOW
    - https://gitlab.com/gitlab-org/gitlab/-/issues/428518
1. [ ] Create deprecation entry for https://gitlab.com/gitlab-org/gitlab/-/issues/422783#note_1757393698?
1. [X] [Update DevSecOps Slides](https://docs.google.com/presentation/d/1XcOmwUvaSEYhj87dvHj05l3t-JuBEcLaFdhSEDvX4EY/edit#slide=id.g2633147b2e9_3_5277)
1. [ ] Start working on a basic roadmap for Compliance - SOC 2 checks, security scan checks, providing more clarity on the plan we've discussed (which also would address the OKR we've discussed).
1. [ ] Prep CS Skills Exchange - https://gitlab.com/gitlab-com/sales-team/cs-skills-exchange/-/issues/177 
1. [ ] Create something to track pipeline of customer calls for both SP and Compliance, so it's visible
1. [X] Progress [License Approval Policy Dogfooding](https://gitlab.com/gitlab-org/gitlab/-/issues/400221) - follow-up now with T Woodham
1. [ ] Get new laptop set up -- follow up with Jeff to wipe old laptop towards end of week if possible
1. [ ] GitLab Summit - confirm activities, unconferences, or anything else I need to prep
1. [ ] Coffee chats with Principal PMs - focus: how to scale myself as a PM
    - 1 more planned this week
1. [X] [Enablement bites](https://gitlab.com/gitlab-com/Product/-/issues/13049) -- leaving this in Sashi/Alan's hands
    - Work with Sashi on a bite for policy accuracy
    - Discuss with Nate what might be useful here for Compliance
1. [ ] Formalize requirements for GA for Pipeline Execution Action -- delegate testing/validation of current state
    - Andy is creating spike issues for each CI behavior we are aware of
    - Can check with Fabio this week to see if he or others can support on this
    - Update table in epic
    - Socialize with CSMs?
    - I'm meeting with Jan who has feedback on the topic, at least for before scripts

Backlog:

1. [ ] Refine notifications UX research
1. [ ] Create a script for testing MR changes?
1. [ ] Watch some CI training videos on various advanced features. (Pick up [here](https://levelup.gitlab.com/learn/course/gitlab-runners/gitlab-runners/getting-started-with-gitlab-runners?client=internal-team-members&page=4))
1. [ ] Set up more workflows for ruthless prioritization and blocking out distractions, find opportunities for delegation
1. [ ] Create a dashboard or tool to track workflow labels and make sure it's easier to track state/progress
1. [ ] Blog post for pipeline execution / policy scoping


## Week of 2024-02-06 (week starting on Tuesday)

Priorities:

1. [X] Create a deck and recorded presentation walking through product direction for 2024 (Security Policies) - wrap up and record, focus on FY25 and save the 3-5 year vision for later
1. [X] Govern Strategy Review
1. [X] Prep MRs for 16.9 (SP & Policies) - opp for more automation?? -- Should have these done by EOW
    - https://gitlab.com/gitlab-org/gitlab/-/issues/428518
1. [X] Progress the Marketing discussion regarding Compliance Pipelines - Meeting planned Feb 8 with Sales
1. [ ] Create deprecation entry for https://gitlab.com/gitlab-org/gitlab/-/issues/422783#note_1757393698
1. [ ] Start working on a basic roadmap for Compliance - SOC 2 checks, security scan checks, providing more clarity on the plan we've discussed (which also would address the OKR we've discussed).
1. [X] Pin down a few compliance focused customer meetings.
1. [X] Plan to highlight feedback issue for customers trying out the experimental features - get issues prioritized
    - Put into docs, UI, etc 
1. [ ] Progress [License Approval Policy Dogfooding](https://gitlab.com/gitlab-org/gitlab/-/issues/400221) - follow-up now with T Woodham
1. [ ] Get new laptop set up -- follow up with Jeff to wipe old laptop towards end of week if possible
1. [ ] GitLab Summit - confirm activities, unconferences, or anything else I need to prep
1. [ ] Coffee chats with Principal PMs - focus: how to scale myself as a PM
    - 1 more planned this week
1. [ ] [Enablement bites](https://gitlab.com/gitlab-com/Product/-/issues/13049)
    - Work with Sashi on a bite for policy accuracy
    - Discuss with Nate what might be useful here for Compliance
1. [ ] Formalize requirements for GA for Pipeline Execution Action -- delegate testing/validation of current state
    - Andy is creating spike issues for each CI behavior we are aware of
    - Can check with Fabio this week to see if he or others can support on this
    - Update table in epic
    - Socialize with CSMs?
    - I'm meeting with Jan who has feedback on the topic, at least for before scripts

Next week:

1. [ ] Refine notifications UX research
1. [ ] Watch some CI training videos on various advanced features. (Pick up [here](https://levelup.gitlab.com/learn/course/gitlab-runners/gitlab-runners/getting-started-with-gitlab-runners?client=internal-team-members&page=4))
1. [ ] Set up more workflows for ruthless prioritization and blocking out distractions, find opportunities for delegation
1. [ ] Create a dashboard or tool to track workflow labels and make sure it's easier to track state/progress
1. [ ] CS Skills Exchange Prep - [Issue](https://gitlab.com/gitlab-com/sales-team/cs-skills-exchange/-/issues/177#note_1750320666)
1. [ ] Blog post for pipeline execution / policy scoping

## Week of 2024-01-30 (week starting on Tuesday)

Priorities:
1. [ ] Create a deck and recorded presentation walking through product direction for 2024 (Security Policies) - Spend two one hour blocks this week and see where I land
1. [ ] Prep MRs for 16.9 (SP & Policies) - opp for more automation??
    - https://gitlab.com/gitlab-org/gitlab/-/issues/428518
    - 
1. [X] Comms to specific CSMs regarding Pipeline Execution Action, get updates channel live in Slack on this topic
    - Create a demo of recent features
    - Share more about the plan for aligning compliance and policies
    - Highlight recently added features for experiments
    - See if I can turn this also into a blog post
1. [X] Plan to highlight feedback issue for customers trying out the experimental features - get issues prioritized
    - Put into docs, UI, etc 
1. [X] Progress [License Approval Policy Dogfooding](https://gitlab.com/gitlab-org/gitlab/-/issues/400221) - follow-ups with D Nelson, then including a few more trial repos
1. [ ] Get new laptop set up -- follow up with Jeff to wipe old laptop towards end of week if possible
1. [ ] GitLab Summit - confirm activities, unconferences, or anything else I need to prep
1. [ ] Coffee chats with Principal PMs - focus: how to scale myself as a PM
    - 1 more planned this week
1. [X] [Enablement bites](https://gitlab.com/gitlab-com/Product/-/issues/13049) - created 2 for policies, but could create a few more
1. [X] Breakdown for Cells work - Security Policies - https://gitlab.com/gitlab-org/gitlab/-/issues/434993 - Delegate candidate? // Alan and team are all over this
1. [ ] Breakdown for Cells work - Compliance - https://gitlab.com/gitlab-org/gitlab/-/issues/434995 - Delegate candidate?
1. [ ] Formalize requirements for GA for Pipeline Execution Action -- delegate testing/validation of current state
    - Andy is creating spike issues for each CI behavior we are aware of
    - Can check with Fabio this week to see if he or others can support on this
    - Update table in epic
    - Socialize with CSMs?
    - I'm meeting with Jan who has feedback on the topic, at least for before scripts
1. [X] Ensure 17.0 deprecations are all accounted for -- Delegate to Alan? // Alan is handling this and it appears we are all set here
    - Go through features released past 6 mo and consider

Next week:
1. [X] Progress the Marketing discussion regarding Compliance Pipelines - Meeting planned Feb 8 with Sales
1. [ ] Refine notifications UX research
1. [ ] Watch some CI training videos on various advanced features. (Pick up [here](https://levelup.gitlab.com/learn/course/gitlab-runners/gitlab-runners/getting-started-with-gitlab-runners?client=internal-team-members&page=4))
1. [ ] Set up more workflows for ruthless prioritization and blocking out distractions, find opportunities for delegation
1. [ ] Create a dashboard or tool to track workflow labels and make sure it's easier to track state/progress



## Week of 2024-01-23 (week starting on Tuesday)

Priorities:

1. [X] Check [this list](https://docs.google.com/spreadsheets/d/1EK4o7HRKNZMpi9CuoWKBO6DhLgILKyRk6d1dFOWR8As/edit#gid=0) and comment
1. [ ] Formalize requirements for GA for Pipeline Execution Action
1. [ ] Comms to specific CSMs regarding Pipeline Execution Action, get updates channel live in Slack on this topic
1. [ ] Plan to highlight feedback issue for customers trying out the experimental features - get issues prioritized
1. [ ] Progress the Marketing discussion regarding Compliance Pipelines
1. [X] Progress [License Approval Policy Dogfooding](https://gitlab.com/gitlab-org/gitlab/-/issues/400221) - follow-ups with D Nelson, then including a few more trial repos
1. [ ] Create a dashboard or tool to track workflow labels and make sure it's easier to track state/progress
1. [ ] Create a deck and recorded presentation walking through product direction for 2024 (Security Policies) - Spend two one hour blocks this week and see where I land
1. [ ] Get new laptop set up -- follow up with Jeff to wipe old laptop towards end of week if possible
1. [ ] Watch some CI training videos on various advanced features. (Pick up [here](https://levelup.gitlab.com/learn/course/gitlab-runners/gitlab-runners/getting-started-with-gitlab-runners?client=internal-team-members&page=4))
1. [ ] Set up more workflows for ruthless prioritization and blocking out distractions, find opportunities for delegation
1. [X] Expenses
1. [ ] GitLab Summit - confirm activities, unconferences, or anything else I need to prep
1. [X] Coffee chats with Principal PMs - focus: how to scale myself as a PM
1. [X] [Enablement bites](https://gitlab.com/gitlab-com/Product/-/issues/13049) - 2 created
1. [X] Ensure 17.0 deprecations are all accounted for

Next week:
1. [ ] Breakdown for Cells work - Security Policies - https://gitlab.com/gitlab-org/gitlab/-/issues/434993
1. [ ] Breakdown for Cells work - Compliance - https://gitlab.com/gitlab-org/gitlab/-/issues/434995
1. [ ] Refine notifications UX research



## Week of 2024-01-16 (week starting on Tuesday)

Priorities:

1. [X] Prep for customer roadmap presentation for Compliance
1. [ ] Formalize requirements for GA for Pipeline Execution Action
1. [ ] Comms to specific CSMs regarding Pipeline Execution Action
1. [X] Plan to highlight feedback issue for customers trying out the experimental features
1. [ ] Progress the Marketing discussion regarding Compliance Pipelines
1. [X] Progress [License Approval Policy Dogfooding](https://gitlab.com/gitlab-org/gitlab/-/issues/400221)
1. [X] Create pre-flight checklist for next steps for LAP Dogfooding
1. [ ] Create a dashboard or tool to track workflow labels and make sure it's easier to track state/progress
1. [ ] Create a deck and recorded presentation walking through product direction for 2024 (Security Policies)
1. [ ] Get new laptop set up
1. [ ] Watch some CI training videos on various advanced features. (Pick up [here](https://levelup.gitlab.com/learn/course/gitlab-runners/gitlab-runners/getting-started-with-gitlab-runners?client=internal-team-members&page=4))
1. [ ] Set up more workflows for ruthless prioritization and blocking out distractions, find opportunities for delegation
1. [ ] Expenses
1. [X] Sort out plans for GitLab Summit
1. [X] Bloom setup
1. [X] Schedule 2-3 coffee chats with Principal PMs

Next week:
1. [ ] [Enablement bites](https://gitlab.com/gitlab-com/Product/-/issues/13049)
1. [ ] Check [this list](https://docs.google.com/spreadsheets/d/1EK4o7HRKNZMpi9CuoWKBO6DhLgILKyRk6d1dFOWR8As/edit#gid=0) and comment
1. [ ] Ensure 17.0 deprecations are all accounted for


## Week of 2024-01-01

Priorities:

1. [X] Prep and share out coverage issue. Ensure clear needs are defined there for the 2nd week of Jan.
    - Prep release post MRs?
    - Coverage for any meetings?
1. [X] Completed "Intro to Cybersecurity" Udacity Nanodegree - yewww!
1. [X] Update direction pages for 16.8 & build a tracking dashboard for the epics
    - Tuesday -- Check prioritization dashboard, check top voted epics, check minimal to viable plans and complete MR
    - Update general direction page content
    - Finalize next week with team - https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/132146/diffs
    - Create a dashboard or tool to track workflow labels and make sure it's easier to track state/progress
1. [X] Next steps on releasing new experimental features
1. [X] Security Policy Dogfooding - ITGC Dogfooding - Sync meeting in new year, consider self-imposing policy requirements // met with Jeff and he will be providing feedback on related epics.
1. [X] Initiate the first full stakeholder sync on [compliance dogfooding efforts](https://gitlab.com/gitlab-org/gitlab/-/issues/400221) with key stakeholders to complete adoption. Set clear engineering DRIs and timelines.
1. [X] Consider feedback for marketing on https://gitlab.com/gitlab-com/marketing/brand-product-marketing/product-marketing/-/issues/7387#note_1689111795 and on ARR topic.
1. [ ] Watch some CI training videos on various advanced features. (Pick up [here](https://levelup.gitlab.com/learn/course/gitlab-runners/gitlab-runners/getting-started-with-gitlab-runners?client=internal-team-members&page=4))

Note: OOO Week of 2023-01-09

## Week of 2023-12-19

Priorities:

1. [X] Escalation support
1. [ ] Update direction pages for 16.8 
    - Tuesday -- Check prioritization dashboard, check top voted epics, check minimal to viable plans and complete MR
    - Update general direction page content
1. [ ] PI updates
1. [ ] Gather/refine customer list for pipeline execution policy action experiment
1. [X] Create a coverage issue for January ski trip
1. [X] Finish IGP (Individual Growth Plan)
1. [ ] Security Policy Dogfooding - ITGC Dogfooding - Sync meeting in new year, consider self-imposing policy requirements
1. [X] Review survey for compliance
1. [ ] Consider feedback for marketing on https://gitlab.com/gitlab-com/marketing/brand-product-marketing/product-marketing/-/issues/7387#note_1689111795 and on ARR topic.
1. [ ] Schedule a sync in the new year around [compliance dogfooding efforts](https://gitlab.com/gitlab-org/gitlab/-/issues/400221) with key stakeholders to complete adoption.
1. [ ] Next week, track SAML SSO escalation for compliance.



## Week of 2023-07-05

Priorities:

1. [ ] Unifying compliance pipelines and security policies -- have started sharing out and need to encourage people to finish the tests, review, then send out more broadly.
1. [ ] Focus on breaking down priorities in a way that is clearer to the engineering team, CSMs, and customers, so they may understand the rationale. -- I created a priority list for customers and features, as well as a strategy map. I need to complete this and make updates to maturity epics and prioritization list.
1. [ ] Prep a few plans following compliance enforcement and unifying compliance pipelines / security policies. We may need to plan some UX or UXR activities to help with validation. Work on organizing plans into the various maturity levels.
1. [ ] Check in on next steps for Legal / Security teams to dogfood. Auto-merge should be usable internally by end of this milestone.
1. [ ] Finalize/prep blog post re: Additional filters for SRPs
1. [ ] Govern PM Interviewing - prep by rewatching training on deep dive interviews, see if there are any I can shadow.
1. [ ] Prep for Sec virtual offsite next week


## Week of 2023-07-05

Priorities:

1. [ ] Unifying compliance pipelines and security policies -- study is ready, sent to two customers first before sending to others. Next step -- encourage them to complete the study, review, gather any remaining candidates and work to send this around.
1. [X] Finalize any remaining release posts.
1. [ ] Focus on breaking down priorities in a way that is clearer to the engineering team, CSMs, and customers, so they may understand the rationale.
1. [ ] Prep a few plans following compliance enforcement and unifying compliance pipelines / security policies. We may need to plan some UX or UXR activities to help with validation. Work on organizing plans into the various maturity levels.
1. [ ] Check in on next steps for Legal / Security teams to dogfood. Auto-merge should be usable internally by end of this milestone.
1. [ ] Finalize/prep blog post re: Additional filters for SRPs


## Week of 2023-07-05

Housekeeping:

- Monday and Tuesday were off for F&F Day and 4th of July, so short week.

Priorities:

1. [ ] Gathering candidates and preparing to send out the user test this week to get feedback on our current designs/prototype.
1. [X] Prepare release posts as we will have several this milestone.
1. [ ] Focus on breaking down priorities in a way that is clearer to the engineering team, CSMs, and customers, so they may understand the rationale.
1. [ ] Prep a few plans following compliance enforcement and unifying compliance pipelines / security policies. We may need to plan some UX or UXR activities to help with validation.
1. [ ] Check in on next steps for Legal / Security teams to dogfood. Auto-merge should be usable internally by end of this milestone.

## Week of 2023-06-05

Housekeeping:

- Planning to attend sessions for virtual PlatformCon on Thu/Fri.

Priorities:

1. [ ] Compliance enforcement of security policies - create an updated walkthrough video, update all issues with test cases / acceptance criteria, and any other risk assessment / info fields, consider any follow up calls with users from the study
1. [ ] `Started` Merging compliance pipelines and security policies -- identify changes to make to existing SV and identify existing customer candidates to re-test with
1. [ ] Check on state of auto-merge. With spammy notifications complete/removed, can see how best to move forward with License Approval dogfooding.
1. [ ] `Started` Consider fail open logic for approval policies. -- create an issue for a toggle option, validate with more customers
1. [ ] `Started` Do an analysis of customers who have tried security policies and did not adopt, see if I can drill down in Sisense for this
1. [ ] `Started` More exploration on pipeline comparison logic for approvals, alignment with Threat Insights - objective is clear understanding of current and future state, any issues broken down to align logic

A few background items:

1. [ ] Do some career planning; set appointments with career coach
1. [ ] `Started` Walkthrough video on exposing GCP instance? Could try adding honeytokens? Share some on GitDorker.
1. [ ] Explore a blog post topic to share any recent learnings on security challenges and how GitLab can help solve them
1. [ ] Elaborate on one or two AI opportunities
1. [ ] Go through immediate issues/epics and organize a view into what's needed from PM on planned issues (workflow board, organized list of my own priorities to prep for dev/refinement)
1. [ ] Review slides (see [DevSecOps Overview](https://docs.google.com/presentation/d/1XcOmwUvaSEYhj87dvHj05l3t-JuBEcLaFdhSEDvX4EY/edit?usp=sharing)) and communicating current features versus top 3 or 4 priorities on the roadmap
1. [ ] Scheduling Coffee Chats with teammates I haven't met with yet
1. [ ] `Started` Complete CEH video
1. [ ] Finish checking out Snyk
1. [ ] Follow-up on heuristic evaluation to plan next steps and any actionable insights; lower priority as Michael will be out starting this week (https://gitlab.com/gitlab-org/ux-research/-/issues/2476)

## Week of 2023-05-22
1. [X] Follow-ups from two SVs for compliance of sec policies, analyze feedback, refine and break down for dev, create a walkthrough video, update all issues with test cases / acceptance criteria, and any other risk assessment / info fields; resend one of the two SVs after fixing sticking points; set any follow-up calls
1. [X] Finish updating direction page to new template, create any tasks needed to fully complete it, e.g. BIC and competitive analysis
1. [ ] Merging compliance pipelines and security policies -- identify changes to make to existing SV and identify existing customer candidates to re-test with
1. [ ] Check on state of auto-merge. With spammy notifications complete/removed, can see how best to move forward with License Approval dogfooding.
1. [ ] [Started] Consider fail open logic for approval policies. -- create an issue for a toggle option, validate with more customers
1. [ ] [Started] Do an analysis of customers who have tried security policies and did not adopt, see if I can drill down in Sisense for this
1. [ ] [Started] More exploration on pipeline comparison logic for approvals, alignment with Threat Insights - objective is clear understanding of current and future state, any issues broken down to align logic
1. [ ] Set some meetings: analyst, team members, customers

A few background items:
1. [ ] Do some career planning; set appointments with career coach
1. [ ] [Started] Walkthrough video on exposing GCP instance? Could try adding honeytokens? Share some on GitDorker.
1. [ ] Explore a blog post topic to share any recent learnings on security challenges and how GitLab can help solve them
1. [ ] Elaborate on one or two AI opportunities
1. [ ] Go through immediate issues/epics and organize a view into what's needed from PM on planned issues (workflow board, organized list of my own priorities to prep for dev/refinement)
1. [ ] [Started] Continue practicing going over slides (see [DevSecOps Overview](https://docs.google.com/presentation/d/1XcOmwUvaSEYhj87dvHj05l3t-JuBEcLaFdhSEDvX4EY/edit?usp=sharing)) and communicating current features versus top 3 or 4 priorities on the roadmap
1. [ ] Scheduling Coffee Chats with teammates I haven't met with yet
1. [ ] Complete CEH video
1. [ ] Finish checking out Snyk
1. [ ] Follow-up on heuristic evaluation to plan next steps and any actionable insights; lower priority as Michael will be out starting this week (https://gitlab.com/gitlab-org/ux-research/-/issues/2476)


## Week of 2023-05-15
1. [ ] More exploration on pipeline comparison logic for approvals, alignment with Threat Insights - objective is clear understanding of current and future state, any issues broken down to align logic
1. [ ] Set some meetings: analyst, team members, customers
1. [ ] Finish updating direction page to new template, create any tasks needed to fully complete it, e.g. BIC and competitive analysis
1. [X] Start running two SVs for compliance of sec policies, analyze feedback, refine and break down for dev, create a walkthrough video, update all issues with test cases / acceptance criteria, and any other risk assessment / info fields
1. [X] Get things moving for merging compliance pipelines and security policies
1. [ ] Check on state of auto-merge. With spammy notifications complete/removed, can see how best to move forward with License Approval dogfooding.
1. [X] With spammy notifications removed, need some quick feedback on MVC approach for lightweight bot comment and could get this planned soon.
1. [ ] Consider fail open logic for approval policies.
1. [ ] Walkthrough video on exposing GCP instance? Could try adding honeytokens? Share some on GitDorker.
1. [ ] Steps to plan heuristic evaluation with Michael.
1. [ ] Do an analysis of customers who have tried security policies and did not adopt, see if I can drill down in Sisense for this


A few background items:
1. [ ] Explore a blog post topic to share any recent learnings on security challenges and how GitLab can help solve them
1. [ ] Create a short tutorial for the GCP VM and monitoring for attacks
1. [ ] Elaborate on one or two AI opportunities
1. [ ] Go through immediate issues/epics and organize a view into what's needed from PM on planned issues (workflow board, organized list of my own priorities to prep for dev/refinement)
1. [ ] [Started] Continue practicing going over slides (see [DevSecOps Overview](https://docs.google.com/presentation/d/1XcOmwUvaSEYhj87dvHj05l3t-JuBEcLaFdhSEDvX4EY/edit?usp=sharing)) and communicating current features versus top 3 or 4 priorities on the roadmap
1. [ ] Scheduling Coffee Chats with teammates I haven't met with yet
1. [ ] Complete CEH video
1. [ ] Finish checking out Snyk

## Week of 2023-04-24
1. [ ] https://gitlab.com/groups/gitlab-org/-/epics/8314 -- problem validation or solution validation? New proposals needed here.
1. [X] Update direction pages
1. [X] Create release posts - prep now for %16.0 release
1. [ ] [Blocked] Taking over Solution Validation for this UX - https://gitlab.com/gitlab-org/ux-research/-/issues/2380 -- waiting on UXR to start this
1. [ ] [Blocked] Survey for Scan Result policy renaming - working with UXR to reformat this
1. [X] Finish first round of troubleshooting tips https://gitlab.com/gitlab-org/gitlab/-/merge_requests/115442
1. [ ] Prep for customer call and ITGC meeting
1. [ ] Do an analysis of customers who have tried security policies and did not adopt, see if I can drill down in Sisense for this

A few background items:
1. [ ] Explore a blog post topic to share any recent learnings on security challenges and how GitLab can help solve them
1. [ ] Create a short tutorial for the GCP VM and monitoring for attacks
1. [ ] Elaborate on one or two AI opportunities
1. [ ] Go through immediate issues/epics and organize a view into what's needed from PM on planned issues (workflow board, organized list of my own priorities to prep for dev/refinement)
1. [ ] [Started] Continue practicing going over slides (see [DevSecOps Overview](https://docs.google.com/presentation/d/1XcOmwUvaSEYhj87dvHj05l3t-JuBEcLaFdhSEDvX4EY/edit?usp=sharing)) and communicating current features versus top 3 or 4 priorities on the roadmap
1. [ ] Scheduling Coffee Chats with teammates I haven't met with yet
1. [ ] [Started] Finish writing down high-level thoughts on [Integrations Strategy](https://docs.google.com/document/d/1NEB0Ua8w8nw0Z6RQNV9h1rW9noOAzL9FHZrAtHpNcno/edit?usp=sharing) to hand off to Urvi, Michelle, and Magda
1. [ ] Finish checking out Snyk

## Week of 2023-04-10

#### Housekeeping
- F&F Day next Monday

#### Priorities
1. [X] License Approval Dogfooding Next Steps -- we made some progress, have a spike and MVC demo for MWPS+approvals. Also an issue to fix spammy notifications. Will revisit with team soon when we know more.
1. [ ] https://gitlab.com/groups/gitlab-org/-/epics/8314 -- walkthrough video and refinement -- this is back to problem or solution validation
1. [ ] Create release posts
1. [ ] [Started] Taking over Solution Validation for this UX - https://gitlab.com/gitlab-org/ux-research/-/issues/2380
1. [ ] Survey for Scan Result policy renaming
1. [X] Per weekly meeting, setup a priority list for Sec partners/integrations, coordinate with Morgan
1. [X] [Started] Review [UXR issue ](https://gitlab.com/gitlab-org/ux-research/-/issues/2223) and a few top interviews that were recorded
1. [ ] Go through immediate issues/epics and organize a view into what's needed from PM on planned issues (workflow board, organized list of my own priorities to prep for dev/refinement)
1. [ ] [Started] Practice going over a few slides (see [DevSecOps Overview](https://docs.google.com/presentation/d/1XcOmwUvaSEYhj87dvHj05l3t-JuBEcLaFdhSEDvX4EY/edit?usp=sharing)) and communicating current features versus top 3 or 4 priorities on the roadmap
1. [ ] Scheduling Coffee Chats with teammates I haven't met with yet
1. [ ] [Started] Finish writing down high-level thoughts on [Integrations Strategy](https://docs.google.com/document/d/1NEB0Ua8w8nw0Z6RQNV9h1rW9noOAzL9FHZrAtHpNcno/edit?usp=sharing) to hand off to Urvi, Michelle, and Magda
1. [ ] Finish checking out Snyk


#### Next Week 
1. 

## Week of 2023-04-03

#### Housekeeping
- N/A

#### Priorities
1. [ ] [Started] Taking over Solution Validation for this UX - https://gitlab.com/gitlab-org/ux-research/-/issues/2380
1. [ ] Survey for Scan Result policy renaming
1. [ ] Customer call Wednesday
1. [ ] [Started] Track progress of https://gitlab.com/gitlab-org/gitlab/-/issues/393962 
1. [ ] [Started] Review [UXR issue ](https://gitlab.com/gitlab-org/ux-research/-/issues/2223) and a few top interviews that were recorded
1. [ ] Go through immediate issues/epics and organize a view into what's needed from PM on planned issues (workflow board, organized list of my own priorities to prep for dev/refinement)
1. [ ] [Started] Practice going over a few slides (see [DevSecOps Overview](https://docs.google.com/presentation/d/1XcOmwUvaSEYhj87dvHj05l3t-JuBEcLaFdhSEDvX4EY/edit?usp=sharing)) and communicating current features versus top 3 or 4 priorities on the roadmap
1. [ ] Scheduling Coffee Chats with teammates I haven't met with yet
1. [ ] [Started] Finish writing down high-level thoughts on [Integrations Strategy](https://docs.google.com/document/d/1NEB0Ua8w8nw0Z6RQNV9h1rW9noOAzL9FHZrAtHpNcno/edit?usp=sharing) to hand off to Urvi, Michelle, and Magda
1. [ ] Finish checking out Snyk


#### Next Week 
1. https://gitlab.com/groups/gitlab-org/-/epics/8314 -- walkthrough video and refinement

## Week of 2023-03-27

#### Housekeeping
- N/A

#### Priorities
1. [X] Sec PI Review
1. [ ] [Started] Taking over Solution Validation for this UX - https://gitlab.com/gitlab-org/ux-research/-/issues/2380
1. [ ] Survey for Scan Result policy renaming
1. [X] Send feedback issue across for License Approval policy dogfooding by GitLab
1. [X] [Started] Start Create collaboration sync with Kai and Torsten
1. [X] [Started] Consider how to move forward with MVCs based on [sync discussion ](https://docs.google.com/document/d/1rEzqW4QXOodoohxkUJ9VZnW0k3mGfzsxzjURd2R_kQA/edit?usp=sharing)with Cam/Sam
1. [ ] [Started] Track progress of https://gitlab.com/gitlab-org/gitlab/-/issues/393962 and call with customer Wednesday
1. [ ] [Started] Review [UXR issue ](https://gitlab.com/gitlab-org/ux-research/-/issues/2223) and a few top interviews that were recorded
1. [ ] Go through immediate issues/epics and organize a view into what's need from PM on planned issues (workflow board, organized list of my own priorities to prep for dev/refinement)
1. [ ] [Started] Practice going over a few slides (see [DevSecOps Overview](https://docs.google.com/presentation/d/1XcOmwUvaSEYhj87dvHj05l3t-JuBEcLaFdhSEDvX4EY/edit?usp=sharing)) and communicating current features versus top 3 or 4 priorities on the roadmap
1. [ ] Scheduling Coffee Chats with teammates I haven't met with yet
1. [ ] [Started] Finish writing down high-level thoughts on [Integrations Strategy](https://docs.google.com/document/d/1NEB0Ua8w8nw0Z6RQNV9h1rW9noOAzL9FHZrAtHpNcno/edit?usp=sharing) to hand off to Urvi, Michelle, and Magda
1. [ ] Finish checking out Snyk


## Week of 2023-03-20

#### Housekeeping
- N/A

#### Priorities
1. [X] [Started] Meet with Kai and Torsten, kick of collaboration and understand relevant issues (met individually)
1. [ ] [Started] Consider how to move forward with MVCs based on [sync discussion ](https://docs.google.com/document/d/1rEzqW4QXOodoohxkUJ9VZnW0k3mGfzsxzjURd2R_kQA/edit?usp=sharing)with Cam/Sam
1. [X] [Started] Track progress of https://gitlab.com/gitlab-org/gitlab/-/issues/393962 and call with customer Wednesday
1. [ ] [Started] Review [UXR issue ](https://gitlab.com/gitlab-org/ux-research/-/issues/2223) and a few top interviews that were recorded
1. [ ] Go through immediate issues/epics and organize a view into what's needed from PM on planned issues (workflow board, organized list of my own priorities to prep for dev/refinement)
1. [X] Consider if/where I can provide test cases and how these need to be designed
1. [ ] [Started] Taking over Solution Validation for this UX - https://gitlab.com/gitlab-org/ux-research/-/issues/2380 -- pinged Michael re: recruitment
1. [ ] [Started] Practice going over a few slides (see [DevSecOps Overview](https://docs.google.com/presentation/d/1XcOmwUvaSEYhj87dvHj05l3t-JuBEcLaFdhSEDvX4EY/edit?usp=sharing)) and communicating current features versus top 3 or 4 priorities on the roadmap
1. [ ] Scheduling Coffee Chats with teammates I haven't met with yet
1. [ ] [Started] Finish writing down high-level thoughts on [Integrations Strategy](https://docs.google.com/document/d/1NEB0Ua8w8nw0Z6RQNV9h1rW9noOAzL9FHZrAtHpNcno/edit?usp=sharing) to hand off to Urvi, Michelle, and Magda
1. [ ] Finish checking out Snyk


## Week of 2023-03-14

#### Housekeeping
- I was off last week. No planned PTO currently for the next several months.

#### Priorities
1. [X] Get caught up on all I've missed
    - Review Marketing materials from meeting last week
    - To-Dos
    - emails
    - Slack threads/DMs
1. [X] Earnings call 
1. [X] Make sure RPs are ready to go
1. [ ] Track progress of https://gitlab.com/gitlab-org/gitlab/-/issues/393962 and call with customer Wednesday
1. [X] Review JTBD findings
    - https://gitlab.com/gitlab-org/ux-research/-/issues/2223#note_1297958236
    - https://www.youtube.com/watch?v=1lPc5eCOSDY
    - https://www.youtube.com/watch?v=aGaFnVOXaAY&t=186s
1. [ ] [Started] Review [UXR issue ](https://gitlab.com/gitlab-org/ux-research/-/issues/2223) and a few top interviews that were recorded
1. [ ] [Started] Practice going over a few slides (see [DevSecOps Overview](https://docs.google.com/presentation/d/1XcOmwUvaSEYhj87dvHj05l3t-JuBEcLaFdhSEDvX4EY/edit?usp=sharing)) and communicating current features versus top 3 or 4 priorities on the roadmap
1. [ ] Revisit priority 7 on down to see if we can pull in more
1. [ ] [Skipping for now] On integrations, work on Roadmap issue to make it a bit cleaner for Magda, based on recent changes
1. [ ] Finish checking out Snyk
1. [ ] [Started] Taking over Solution Validation for this UX - https://gitlab.com/gitlab-org/ux-research/-/issues/2380 -- pinged Michael re: recruitment
1. [ ] Scheduling Coffee Chats with teammates I haven't met with yet
1. [ ] [Started] Finish writing down high-level thoughts on [Integrations Strategy](https://docs.google.com/document/d/1NEB0Ua8w8nw0Z6RQNV9h1rW9noOAzL9FHZrAtHpNcno/edit?usp=sharing) to hand off to Urvi, Michelle, and Magda
1. [ ] Consider how to move forward with MVCs based on[ sync discussion ](https://docs.google.com/document/d/1rEzqW4QXOodoohxkUJ9VZnW0k3mGfzsxzjURd2R_kQA/edit?usp=sharing)with Cam/Sam

## Week of 2023-02-28

#### Housekeeping
- Monday was a F&F Day
- Next week I'll be on PTO for the week
- I'm transitioning from Manage:Integrations to Govern:Security Policies so my priorities are changing

#### Priorities
1. [X] Review relevant videos on Security Policies, conrinue reviewing transition doc and get familiar with all resources
1. [ ] [Started] Practice going over a few slides (see [DevSecOps Overview](https://docs.google.com/presentation/d/1XcOmwUvaSEYhj87dvHj05l3t-JuBEcLaFdhSEDvX4EY/edit?usp=sharing)) and communicating current features versus top 3 or 4 priorities on the roadmap
1. [ ] [Started] Review [UXR issue ](https://gitlab.com/gitlab-org/ux-research/-/issues/2223) and a few top interviews that were recorded
1. [X] Create an epic to remove License Check
1. [ ] Revisit priority 7 on down to see if we can pull in more
1. [X] RP for role based approvals, started adding in https://gitlab.com/groups/gitlab-org/-/epics/8018
1. [X] RP for SAST IaC in SEP, based on https://about.gitlab.com/releases/2022/11/22/gitlab-15-6-released/#scan-execution-policy-support-for-dependency-scanning. [Thread](https://gitlab.slack.com/archives/CU9V380HW/p1677101417190569).
1. [ ] On integrations, work on Roadmap issue to make it a bit cleaner for Magda, based on recent changes
1. [ ] Help with triage and RPs for integrations
1. [ ] Finish checking out Snyk

#### Some goals to consider after my PTO
1. [ ] [Started] Taking over Solution Validation for this UX - https://gitlab.com/gitlab-org/ux-research/-/issues/2380 -- pinged Michael re: recruitment
1. [ ] Scheduling Coffee Chats with teammates I haven't met with yet
1. [ ] [Started] Finish writing down high-level thoughts on [Integrations Strategy](https://docs.google.com/document/d/1NEB0Ua8w8nw0Z6RQNV9h1rW9noOAzL9FHZrAtHpNcno/edit?usp=sharing) to hand off to Urvi, Michelle, and Magda
1. [ ] Consider how to move forward with MVCs based on[ sync discussion ](https://docs.google.com/document/d/1rEzqW4QXOodoohxkUJ9VZnW0k3mGfzsxzjURd2R_kQA/edit?usp=sharing)with Cam/Sam

## Week of 2023-01-30
1. [ ] [Started] Opportunity Canvas Lite for Webhooks - in review, incorporate feedback and close
1. [ ] [Started] Complete integration [transition plan](https://gitlab.com/groups/gitlab-org/-/epics/9699)
1. [ ] [Started] Get more insight on Automation PoC and Event strategies
1. [ ] Update Direction pages based on Vision Alignment issue
1. [ ] Prepare for time off and work ahead for next milestone planning, release posts
1. [ ] Send follow-ups on integrations survey Interviews
1. [ ] Send out interview requests regarding Jira churn
1. [ ] Release posts for %15.9 -- this should primarily be the Slack release
1. [ ] Opportunity Canvas Lite for Integrations Static DSL - need to complete
1. [ ] Define any further Jira proxy deprecation/migration -- migration guide? Docs updates clarifying feature parity?
1. [ ] Jira proxy Deprecation messaging -- set up some communication to share through CSMs
1. [ ] [Continue - This will be a WIP over multiple milestones] Roadmap Epic
1. [ ] Refine my workflow for refining/prioritizing issues (using RICE score, etc), show priorities in each category
1. [ ] Highlight top impacting bugs and refine
1. [ ] Create an issue with some ideas for a Platform Roundtable


## Week of 2023-01-23
1. [X] Finish PI review
1. [X] Conclude on integration PI metrics
1. [X] Vision alignment needs
1. [ ] [Started] Opportunity Canvas Lite for Webhooks - in review, incorporate feedback and close
1. [ ] [Started] Complete integration [transition plan](https://gitlab.com/groups/gitlab-org/-/epics/9699)
1. [ ] Send follow-ups on integrations survey Interviews
1. [ ] Send out interview requests regarding Jira churn
1. [ ] Release posts for %15.9 -- this should primarily be the Slack release
1. [ ] [Started] Get more insight on Automation PoC and Event strategies
1. [ ] Opportunity Canvas Lite for Integrations Static DSL - need to complete
1. [ ] Define any further Jira proxy deprecation/migration -- migration guide? Docs updates clarifying feature parity?
1. [ ] Jira proxy Deprecation messaging -- set up some communication to share through CSMs
1. [ ] [Continue - This will be a WIP over multiple milestones] Roadmap Epic
1. [ ] Refine my workflow for refining/prioritizing issues (using RICE score, etc), show priorities in each category
1. [ ] Highlight top impacting bugs and refine


## Week of 2023-01-02

** OOO Monday and Friday of this week

1. [X] Get Jira blog post published
1. [ ] Opportunity Canvas Lite for Webhooks - in review, iterate and close
1. [ ] Opportunity Canvas Lite for Integrations Static DSL - need to complete
1. [X] [Started] Do a GitLab Learn module on CI, try setting up my own test pipelines
1. [ ] Send follow-ups on integrations survey Interviews
1. [X] Send out interview requests regarding Jira churn
1. [ ] Release posts for %15.8 -- this should primarily be the Slack release
1. [ ] Define any further Jira proxy deprecation/migration -- migration guide? Docs updates clarifying feature parity?
1. [ ] Jira proxy Deprecation messaging
1. [X] Refinement of outbound filtering, w/ timeline
1. [ ] Roadmap Epic
1. [ ] Refine my workflow for refining/prioritizing issues (using RICE score, etc), show priorities in each category
1. [ ] Highlight top impacting bugs and refine

Next: 
1. [ ] Complete GDK Setup
1. [ ] Integrations strategy guide Draft
1. [ ] Review/close open tabs to get organized


## Week of 2022-12-12
1. [ ] Prep any additional release posts for %15.8
1. [X] Test Jira Proxy (note: wasn't able to test the transitions yet)
1. [ ] Define any further Jira proxy deprecation/migration -- migration guide? Docs updates clarifying feature parity?
1. [ ] Opportunity Canvas Lite for Webhooks - in review, iterate and close
1. [ ] Jira proxy Deprecation messaging
1. [ ] Review/close open tabs to get organized
1. [ ] [Started] Do a GitLab Learn module on CI, try setting up my own test pipelines
1. [ ] Refinement of outbound filtering, w/ timeline
1. [ ] Roadmap Epic
1. [ ] Define my workflow for refining/prioritizing issues (using RICE score, etc)
1. [ ] Opportunity Canvas Lite for Integrations Static DSL - need to complete
1. [ ] GDK Setup
1. [ ] Integrations strategy guide Draft

After holidays planning:
1. [ ] Send follow-ups on integrations survey Interviews
1. [ ] Send out interview requests regarding Jira churn


## Week of 2022-12-12
1. [X] GitLab for Jira app Release Post
1. [X] GitLab for Jira app [Blog Post](https://docs.google.com/document/d/1_WRJP_xu24Hie4Gt5RmnGIDMpHWCZHGx6KRahWI0zTA/edit?usp=sharing) -- prepping for %15.8 -- really this just needs to be ready before Jan 2, not Dec 22
1. [X] %15.8 planning
1. [ ] Prep any additional release posts for %15.8
1. [ ] Jira proxy Deprecation messaging
1. [ ] Define any further Jira proxy deprecation/migration -- migration guide? Docs updates clarifying feature parity?
1. [X] PIE Interviews
1. [X] CDF Review
1. [ ] Refinement of outbound filtering, w/ timeline
1. [ ] Roadmap Epic
1. [ ] Define my workflow for refining/prioritizing issues (using RICE score, etc)
1. [X] Finish Enterprise Integrations Survey and share out
1. [ ] Opportunity Canvas Lite for Webhooks - in review, iterate and close
1. [ ] Opportunity Canvas Lite for Integrations Static DSL - need to complete
1. [ ] GDK Setup
1. [ ] Test Jira Proxy
1. [ ] Integrations strategy guide Draft



## Week of 2022-11-21
1. [X] Product PI Review
1. [X] Review Enterprise Tools Survey and create Actionable Insights issue
1. [ ] Opportunity Canvas Lite for Webhooks - in review, iterate and close
1. [ ] Opportunity Canvas Lite for Integrations Static DSL - need to complete
1. [X] Follow-up on customer collab issue (grocery store customer)
1. [ ] [Started] Refine 2-3 issues around webhooks, Jira, and ServiceNow each
1. [ ] Prep release posts for %15.8 in advance
1. [ ] [Started] Explore some processes/routines for deeper backlog planning, refinement, RICE scoring, and roadmapping tools - focus on clarifying mid-term planning
1. [X] Retro and adjustments to the milestone planning, release plans to address monthly frustrations
1. [ ] [90%] GitLab for Jira App Release Post, [Blog Post](https://docs.google.com/document/d/1_WRJP_xu24Hie4Gt5RmnGIDMpHWCZHGx6KRahWI0zTA/edit?usp=sharing) -- prepping for %15.8
1. [ ] Set up some steady streams of customer candidates for calls as suggested in 1:1 with [Katie](https://docs.google.com/document/d/171MelgQUNwkoSBjX1s66NU1CQ0jL_EmEbLFEk1CdWPo/edit?usp=sharing).
1. [X] Schedule some calls - Michelle, Christen
1. [ ] Define Integrations PI metrics moving forward and plan for instrumentation
1. [ ] Integrations strategy guide Draft


## Week of 2022-11-14
1. [X] Milestone Planning - %15.7
1. [X] Release posts
1. [ ] Follow-up on customer collab issue (grocery store customer)
1. [X] [Set up Q4 OKRs](https://gitlab.com/gitlab-org/manage/integrations/team/-/issues/177)
1. [ ] Review internal API survey and share with team
1. [ ] Opportunity Canvas Lite for Webhooks - in review
1. [ ] Opportunity Canvas Lite for Integrations Static DSL - need to complete
1. [ ] GitLab for Jira App Release Post, [Blog Post](https://docs.google.com/document/d/1_WRJP_xu24Hie4Gt5RmnGIDMpHWCZHGx6KRahWI0zTA/edit?usp=sharing) -- prepping for %15.7
1. [ ] Set up some steady streams of customer candidates for calls as suggested in 1:1 with [Katie](https://docs.google.com/document/d/171MelgQUNwkoSBjX1s66NU1CQ0jL_EmEbLFEk1CdWPo/edit?usp=sharing).
1. [ ] Schedule some calls - Michelle, Christen
1. [ ] Schedule internal teams to discuss handing off integrations, gather feedback
1. [ ] Explore Product process for rollout process and customer impact, to identify in issues upfront
1. [ ] Explore some processes/routines for deeper backlog planning, refinement, RICE scoring, and roadmapping tools - focus on clarifying mid-term planning
1. [ ] Define Integrations PI metrics moving forward and plan for instrumentation
1. [ ] Look into product events / conferences that would be relevant for upskilling / education budget
1. [ ] Integrations strategy guide Draft


Recurring Weekly:
1. [ ] Connect with at least 2 Customers/Partners each week - 2 calls planned this week
1. [ ] 1 PM Peer Interview per week
1. [ ] Test at least 1 new integration each week (Plan: explore Slack App Manifests)


## Week of 2022-11-08
1. [ ] Opportunity Canvas Lite for Webhooks - in review
1. [ ] Opportunity Canvas Lite for Integrations Static DSL - need to complete
1. [ ] Release posts
1. [ ] GitLab for Jira App Release Post, [Blog Post](https://docs.google.com/document/d/1_WRJP_xu24Hie4Gt5RmnGIDMpHWCZHGx6KRahWI0zTA/edit?usp=sharing)
1. [ ] Follow-up on customer collab issue (grocery store customer)
1. [ ] Set up some steady sreams of customer candidates for calls as suggested in 1:1 with [Katie](https://docs.google.com/document/d/171MelgQUNwkoSBjX1s66NU1CQ0jL_EmEbLFEk1CdWPo/edit?usp=sharing).
1. [ ] Schedule some calls - Michelle, Christen
1. [ ] Schedule internal teams to discuss handing off integrations, gather feedback
1. [ ] Explore Product process for rollout process and customer impact, to identify in issues upfront
1. [ ] Look into product events / conferences that would be relevant for upskilling / education budget
1. [ ] Explore some processes/routines for deeper backlog planning, refinement, RICE scoring, and roadmapping tools - focus on clarifying mid-term planning
1. [ ] [Set up Q4 OKRs](https://gitlab.com/gitlab-org/manage/integrations/team/-/issues/177)
1. [ ] Define Integrations PI metrics moving forward and plan for instrumentation


Recurring Weekly:
1. [ ] Connect with at least 2 Customers/Partners each week (Focus: API Users, Webhook users, ServiceNow, Jira VSM cases)
1. [ ] 1 PM Peer Interview per week
1. [ ] Test at least 1 new integration each week (Plan: explore Slack App Manifests)


## Week of 2022-10-26

Note: PTO 10-19 to 10-25, short week


1. [X] Opportunity Canvas Lite for Webhooks
1. [ ] Work on better separation of responsibilities between Product and Engineering, make sure I'm prioritizing my own tasks well
1. [ ] Set up some steady sreams of customer candidates for calls as suggested in 1:1 with [Katie](https://docs.google.com/document/d/171MelgQUNwkoSBjX1s66NU1CQ0jL_EmEbLFEk1CdWPo/edit?usp=sharing).
1. [ ] Follow-up on customer collab issue (grocery store customer)
1. [X] Team Topologies completed
1. [ ] Explore some processes/routines for deeper backlog planning, refinement, RICE scoring, and roadmapping tools - focus on clarifying mid-term planning
1. [ ] Explore Product process for rollout process and customer impact, to identify in issues upfront
1. [ ] Set up regular call with Christen
1. [ ] Set up calls with internal teams to discuss ownership of Integrations within their domain (Sam Kerr re: SNOW, Gabe re: Jira, Kevin re: Datadog)
1. [X] Add integrations table to Direction Page
1. [ ] Prioritization table - PI metrics, PI followup questions
1. [ ] Schedule call with Michelle
1. [ ] Look into product events / conferences that would be relevant for upskilling / education budget

Recurring Weekly:
1. [ ] Connect with at least 2 Customers/Partners each week (Focus: API Users, ServiceNow, Jira VSM cases) -- 1 customer call through UXR completed
1. [ ] 1 PM Peer Interview per week
1. [ ] Test at least 1 new integration each week (Plan: explore Slack App Manifests)


## Week of 2022-10-03

1. [X] Share YT video on Direction change, share high level in PM meeting
1. [ ] Work on several Opportunity Canvas Lite issues - Integrations Catalog, Webhook Improvements, Integrations DSL
1. [ ] Integrations UXR - Primary Tools Enterprise Customers depend on / use alongside GitLab - Status: currently iterating and meeting with customers to walkthrough the survey before sending
1. [ ] GitLab for Jira App Release Post, [Blog Post](https://docs.google.com/document/d/1_WRJP_xu24Hie4Gt5RmnGIDMpHWCZHGx6KRahWI0zTA/edit?usp=sharing)
1. [ ] SNOW Spoke video / blog post
1. [X] Section II of Team Topologies
1. [ ] Explore some processes/routines for deeper backlog planning, refinement, RICE scoring, and roadmapping tools - focus on clarifying mid-term planning
1. [ ] Explore Product process for rollout process and customer impact, to identify in issues upfront

Recurring Weekly:
1. [ ] Connect with at least 2 Customers/Partners each week (Focus: API Users, ServiceNow, Jira VSM cases) -- 1 customer call through UXR completed
1. [ ] 1 PM Peer Interview per week
1. [ ] Test at least 1 new integration each week (Plan: explore Slack App Manifests)

## Week of 2022-09-26

1. [X] PI Metrics Review
1. [X] Direction change communication - [Slides](https://docs.google.com/presentation/d/1Xd0a9JGiE97GWuw5OEAvIKOQJ7JqeZGLo44CfX7ymb4/edit#slide=id.g151b88d2ebe_0_19) - Prep for sharing in Stage PM meeting
1. [X] Direction change communication - walkthrough video
1. [X] [API Gap Analysis Survey](https://gitlab.com/gitlab-org/gitlab/-/issues/363615) -- completed, but need to review one more time and send to appropriate channels
1. [ ] Integrations UXR - Primary Tools Enterprise Customers depend on / use alongside GitLab - Status: currently iterating and meeting with customers to walkthrough the survey before sending
1. [ ] Integrations Catalog - OC Lite
1. [ ] SNOW Spoke video / blog post
1. [ ] GitLab for Jira App Release Post, [Blog Post](https://docs.google.com/document/d/1_WRJP_xu24Hie4Gt5RmnGIDMpHWCZHGx6KRahWI0zTA/edit?usp=sharing)
1. [ ] Section II of Team Topologies
1. [ ] Explore some processes/routines for deeper backlog planning, refinement, and roadmapping tools - focus on clarify in mid-term planning

Recurring Weekly:
1. [X] Connect with at least 2 Customers/Partners each week (Focus: API Users, ServiceNow, Jira VSM cases) -- Met with 2 partners, 1 other customer call booked (eek, they no-showed)
1. [X] 1 PM Peer Interview per week -- met w/ Gabe/Christen to walk through Direction change
1. [X] Test at least 1 new integration each week (Plan: explore Slack App Manifests) -- explored Slack App manifests but will want to continue this

## Week of 2022-09-19

1. [X] Kickoff Video
1. [ ] Direction change communication - [Slides](https://docs.google.com/presentation/d/1Xd0a9JGiE97GWuw5OEAvIKOQJ7JqeZGLo44CfX7ymb4/edit#slide=id.g151b88d2ebe_0_19) - Prep for sharing in Stage PM meeting
1. [ ] Direction change communication - walkthrough video
1. [ ] [API Gap Analysis Survey](https://gitlab.com/gitlab-org/gitlab/-/issues/363615) -- completed, but need to review one more time and send to appropriate channels
1. [ ] Integrations UXR - Primary Tools Enterprise Customers depend on / use alongside GitLab
1. [ ] Integrations Catalog - OC Lite
1. [ ] SNOW Spoke video / blog post
1. [ ] GitLab for Jira App Release Post, Blog Post
1. [ ] Section II of Team Topologies
1. [ ] Explore some processes/routines for deeper backlog planning, refinement, and roadmapping tools

Recurring Weekly:
1. [ ] Connect with at least 2 Customers each week (Focus: API Users, ServiceNow, Jira VSM cases) (Call with customer booked)
1. [ ] 1 PM Peer Interview per week (Planned chat with Fabian)
1. [ ] Test at least 1 new integration each week (Plan: explore Slack App Manifests)

## Week of 2022-09-12

1. [X] Finalize Planning
1. [ ] Kickoff Video
1. [ ] Direction change communication - [Slides](https://docs.google.com/presentation/d/1Xd0a9JGiE97GWuw5OEAvIKOQJ7JqeZGLo44CfX7ymb4/edit#slide=id.g151b88d2ebe_0_19) - Prep for sharing in Stage PM meeting
1. [ ] Direction change communication - walkthrough video
1. [ ] [API Gap Analysis Survey](https://gitlab.com/gitlab-org/gitlab/-/issues/363615)
1. [ ] Integrations UXR - Primary Tools Enterprise Customers depend on / use alongside GitLab
1. [ ] Integrations Catalog - OC Lite
1. [ ] SNOW Spoke video
1. [ ] Section II of Team Topologies
1. [ ] Explore some processes/routines for deeper backlog planning, refinement, and roadmapping tools

Recurring Weekly:
1. [ ] Connect with at least 2 Customers each week (Focus: API Users, ServiceNow, Jira VSM cases)
1. [ ] 1 PM Peer Interview per week
1. [X] Test at least 1 new integration each week - Slack workflow builder

## Week of 2022-08-28

Note: Out Monday Aug 28th for F&F Day

1. [ ] Direction change communication - [Slides](https://docs.google.com/presentation/d/1Xd0a9JGiE97GWuw5OEAvIKOQJ7JqeZGLo44CfX7ymb4/edit#slide=id.g151b88d2ebe_0_19)
1. [ ] Direction change communication - walkthrough video
1. [ ] Direction Page MR - add supporting data/diagrams and move along approvals
1. [X] Release Posts Finalized - [Hangouts contribution](https://gitlab.com/gitlab-org/gitlab/-/issues/27823#note_1069831067), Jira Proxy for Self-Managed, Slack, [Shimo](https://gitlab.com/gitlab-org/gitlab/-/issues/345356), [Disabled Webhooks](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/95998#note_1074823737)
1. [ ] Integrations UXR - Primary Tools Enterprise Customers depend on / use alongside GitLab
1. [ ] Integrations Catalog - OC Lite
1. [ ] Section II of Team Topologies
1. [X] SNOW review/enablement meeting
1. [ ] [API Gap Analysis Survey](https://gitlab.com/gitlab-org/gitlab/-/issues/363615)

Recurring Weekly:
1. [ ] Connect with at least 2 Customers each week (Focus: API Users, ServiceNow, Jira VSM cases)
1. [ ] 1 PM Peer Interview per week
1. [X] Test at least 1 new integration each week (Tested SNOW Spoke)

## Week of 2022-08-22

This week:
1. [X] Release Posts Prep - [Hangouts contribution](https://gitlab.com/gitlab-org/gitlab/-/issues/27823#note_1069831067), Jira Proxy for Self-Managed, Slack, [Shimo](https://gitlab.com/gitlab-org/gitlab/-/issues/345356), [Disabled Webhooks](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/95998#note_1074823737)
1. [ ] `Started` Direction Page MR - add supporting data/diagrams and move along approvals
1. [ ] Integrations UXR - Primary Tools Enterprise Customers depend on / use alongside GitLab
1. [ ] Integrations Catalog - OC Lite
1. [ ] `Started` Section II of Team Topologies
1. [ ] `Started` SNOW review and meeting planning 
1. [ ] `Started` [API Gap Analysis Survey](https://gitlab.com/gitlab-org/gitlab/-/issues/363615)
1. [X] Start a round of meetings with team members, define questions to capture feedback on Direction and state of the team

Recurring Weekly:
1. [ ] Connect with at least 2 Customers each week (Focus: API Users, ServiceNow)
1. [X] 1 PM Peer Interview per week
1. [ ] Test at least 1 new integration each week

## Week of 2022-08-15

This week:
1. [X] Finalize %15.4 Planning
1. [ ] Release Posts (if any)
1. [ ] Direction Page MR - add supporting data/diagrams and move along approvals
1. [ ] Integrations UXR - Primary Tools Enterprise Customers depend on / use alongside GitLab
1. [ ] Integrations Catalog - OC Lite
1. [X] Read first few chapters of Team Topologies
1. [X] Prep for next API Working Group 
1. [ ] Review SNOW Spoke

Recurring Weekly:
1. [ ] Connect with at least 2 Customers each week (Focus: API Users, ServiceNow)
1. [X] 1 PM Peer Interview per week (Discussed Slack Integration w/ Alana)
1. [ ] Test at least 1 new integration each week

## Week of 2022-07-25

Note: will be on PTO Aug 1 - Aug 12

This week:
1. [X] OKRs 
1. [X] %15.4 Planning ahead of PTO
1. [ ] Prep on any release posts
1. [X] Mid-year check in 
1. [ ] API Working Group - Prioritization/next steps on Raisin POC
1. [ ] Direction Page MR - add supporting data/diagrams and move along approvals
1. [ ] Read first few chapters of Team Topologies

Recurring Weekly:
1. [ ] Connect with at least 2 Customers each week (Focus: API Users, ServiceNow)
1. [ ] 1 PM Peer Interview per week 
1. [X] Test at least 1 new integration each week - Test integrating Slack Notifications with Slack App

## Week of 2022-07-11

This week:
1. [X] API Working Group - Refine/plan short ~10m presentation for Product group
1. [ ] API Working Group - Prioritization/next steps on Raisin POC
1. [X] Setting up calls for SNOW and additional API Calls
1. [X] Direction Page Update (consider DRIs strategy) - add supporting data/diagrams
1. [ ] Basic Integrations RICE
1. [ ] Jira/Slack refinement - top 3 bugs, top 3 issues
1. [X] Mentorship planning
1. [X] PI Updates
1. [ ] Read first few chapters of Team Topologies

Recurring Weekly:
1. [X] Connect with at least 2 Customers each week (Focus: API Users, ServiceNow) - spoke with customer re SNOW, and another re Slack for Self-Managed
1. [ ] 1 PM Peer Interview per week - meeting moved
1. [ ] Test at least 1 new integration each week

## Week of 2022-07-04

This week:
1. [X] API Working Group - Formulate a short presentation or video speaking through the progress of the API Working Group and next steps / open topics 
1. [ ] API Working Group - Prioritization/next steps on Raisin POC
1. [ ] Setting up calls for SNOW and additional API Calls
1. [X] Direction Page Update (consider DRIs strategy)
1. [X] Enforcement of rate limits - next steps
1. [ ] Basic Integrations RICE

Recurring Weekly:
1. [ ] Connect with at least 2 Customers each week (Focus: API Users)
1. [X] 1 PM Peer Interview per week
1. [ ] Test at least 1 new integration each week


## Week of 2022-06-28

This week:
1. [ ] Jira & Slack planning/refinement - Jira Issue Analytics in VSM, Jira bi-directional sync, Slack DMs, Slack Granular Notifications; come up with a better way of showing why, such as a clean RICE spreadsheet
1. [ ] `Started` Create my own high-level view of progress (similar to Work in Progress Epics, or leveraging other GitLab features)
1. [ ] `Started` Document 2-3 ServiceNow Use Cases and validate with 2-3 customers (consider some low fidelity designs)
1. [ ] `Started` Formulate a short presentation or video speaking through the progress of the API Working Group and next steps / open topics 
1. [ ] Direction Page Update (consider DRIs strategy)
1. [X] Alliance collaboration iteration proposal
1. [ ] Explore external PM Coach
1. [ ] `Started` Prioritization/next steps on Raisin POC
1. [ ] Check on Product Metrics with data model update
1. [ ] Enforcement of rate limits - next steps

Recurring Weekly:
1. [ ] Connect with at least 2 Customers each week (Focus: API Users)
1. [ ] 1 PM Peer Interview per week
1. [ ] Test at least 1 new integration each week

## Week of 2022-06-13
This week:
1. [X] `Started` Propose a prioritization schema for API Working Group / proposal for REST/GraphQL
1. [ ] Jira & Slack planning/refinement - Jira Issue Analytics in VSM, Jira bi-directional sync, Slack DMs, Slack Granular Notifications
1. [X] Slack prep for Slack Frontiers / finalizing Marketplace soft release
1. [ ] `Started` Document 2-3 ServiceNow Use Cases and validate with 2-3 customers (consider some low fidelity designs)
1. [ ] `Started` Create my own high-level view of progress (similar to Work in Progress Epics, or leveraging other GitLab features)
1. [X] Round out questions and share issue on Marketplaces/Plugins with team
1. [X] Explore PM Coaching - found a mentor! But could keep working on external PM coach

Recurring Weekly:
1. [ ] Connect with at least 2 Customers each week (Focus: API Users)
1. [ ] 1 PM Peer Interview per week
1. [ ] Test at least 1 new integration each week

Paused/Blocked:
1. [ ] Review existing API Observability tools/issues, document what we have and how we could use it, and create a gap analysis of what is missing
1. [ ] Socialize go-gitlang issue

## Week of 2022-06-06
This week:
1. [X] %15.2 Prep/Planning
1. [X] DVCS Deprecation Planning
1. [ ] `Started` Document 2-3 ServiceNow Use Cases and validate with 2-3 customers (consider some low fidelity designs)
1. [ ] `Started` Propose a prioritization schema for API Working Group / proposal for REST/GraphQL
1. [ ] `Started` Create my own high-level view of progress (similar to Work in Progress Epics, or leveraging other GitLab features)
1. [ ] Slack prep for Slack Frontiers / finalizing Marketplace soft release
1. [ ] Round out questions and share issue on Marketplaces/Plugins with team
1. [ ] Jira & Slack planning/refinement - Jira Issue Analytics in VSM, Jira bi-directional sync, Slack DMs, Slack Granular Notifications
1. [ ] Explore PM Coaching

Recurring Weekly:
1. [X] Connect with at least 2 Customers each week (Focus: API Users)
1. [ ] 1 PM Peer Interview per week
1. [ ] Test at least 1 new integration each week

Paused/Blocked:
1. [ ] Review existing API Observability tools/issues, document what we have and how we could use it, and create a gap analysis of what is missing
1. [ ] Socialize go-gitlang issue


## Week of 2022-05-31
This week:
1. [ ] DVCS Deprecation Planning
1. [ ] Slack prep for Slack Frontiers / finalizing Marketplace soft release
1. [ ] `Started` Propose a prioritization schema for API Working Group / proposal for REST/GraphQL
1. [ ] Round out questions and share issue on Marketplaces/Plugins with team
1. [ ] Document 2-3 ServiceNow Use Cases and validate with 2-3 customers (consider some low fidelity designs)
1. [ ] Jira planning/refinement
1. [ ] Socialize go-gitlang issue
1. [ ] `Started` Review existing API Observability tools/issues, document what we have and how we could use it, and create a gap analysis of what is missing
1. [ ] Create my own high-level view of progress (similar to Work in Progress Epics, or leveraging other GitLab features)
1. [ ] Explore PM Coaching

Recurring Weekly:
1. [ ] Connect with at least 2 Customers each week (Focus: API Users) - no meetings but lots of work on the pipeline to get these calls scheduled!
1. [X] 1 PM Peer Interview per week - met with Haim and discussed Jira VSM
1. [X] Test at least 1 new integration each week - Revisit of Jira, test of Slack GBP App w/ walkthrough

## Week of 2022-05-23
** Note: OOO Friday, May 27th and Monday, May 30th

This week:
1. [ ] DVCS Deprecation Planning
1. [ ] `Started` Propose a prioritization schema for API Working Group / proposal for REST/GraphQL
1. [ ] Round out questions and share issue on Marketplaces/Plugins with team
1. [ ] `Started` Document 2-3 ServiceNow Use Cases and validate with 2-3 customers (consider some low fidelity designs)
1. [ ] Jira planning/refinement
1. [ ] Socialize go-gitlang issue
1. [ ] `Started` Review existing API Observability tools/issues, document what we have and how we could use it, and create a gap analysis of what is missing
1. [ ] Create my own high-level view of progress (similar to Work in Progress Epics, or leveraging other GitLab features)
1. [ ] Explore PM Coaching

Recurring Weekly:
1. [ ] [1/2] Connect with at least 2 Customers each week (Focus: API Users)
1. [ ] 1 PM Peer Interview per week
1. [ ] Test at least 1 new integration each week

## Week of 2022-05-16
** Note: Traveling this week, working Async Tuesday.

This week:
1. [ ] `Blocked - Waiting on Atlassian` DVCS Deprecation Planning
1. [ ] `Started` Propose a prioritization schema for API Working Group / proposal for REST/GraphQL
1. [ ] Socialize go-gitlang issue
1. [ ] Round out questions and share issue on Marketplaces/Plugins with team
1. [ ] Document 2-3 ServiceNow Use Cases and validate with 2-3 customers (consider some low fidelity designs)
1. [ ] `Started` Review existing API Observability tools/issues, document what we have and how we could use it, and create a gap analysis of what is missing
1. [ ] Create my own high-level view of progress (similar to Work in Progress Epics, or leveraging other GitLab features)
1. [ ] Explore PM Coaching


Recurring Weekly:
1. [ ] Connect with at least 2 Customers each week (Focus: API Users)
1. [ ] 1 PM Peer Interview per week
1. [ ] Test at least 1 new integration each week

## Week of 2022-05-09

This week:
1. [ ] `Blocked - Waiting on Atlassian` DVCS Deprecation Planning 
1. [ ] Socialize go-gitlang issue
1. [ ] `Started` Propose a prioritization schema for API Working Group / proposal for REST/GraphQL
1. [ ] Round out questions and share issue on Marketplaces/Plugins with team
1. [ ] Document 2-3 ServiceNow Use Cases and validate with 2-3 customers (consider some low fidelity designs)
1. [ ] `Started` Review existing API Observability tools/issues, document what we have and how we could use it, and create a gap analysis of what is missing
1. [X] %15.1 Planning Finalization
1. [ ] Explore a few VSM Tools
1. [ ] Create my own high-level view of progress (similar to Work in Progress Epics, or leveraging other GitLab features)
1. [ ] Explore PM Coaching


Recurring Weekly:
1. [X] [1/2] Connect with at least 2 Customers each week (Focus: API Users)
1. [X] 1 PM Peer Interview per week
1. [ ] Test at least 1 new integration each week

## Week of 2022-05-02

This week:
1. [ ] `Blocked - Waiting on Atlassian` DVCS Deprecation Planning 
1. [X] `Started` Finish organization of Slack Epic / Share Dovetail insights
1. [ ] `Started` Review existing API Observability tools/issues, document what we have and how we could use it, and create a gap analysis of what is missing
1. [ ] Document 2-3 ServiceNow Use Cases and validate with 2-3 customers (consider some low fidelity designs)
1. [ ] `Started` Propose a prioritization schema for API Working Group / proposal for REST/GraphQL
1. [ ] Round out questions and share issue on Marketplaces/Plugins with team
1. [ ] Explore a few VSM Tools
1. [ ] Create my own high-level view of progress (similar to Work in Progress Epics, or leveraging other GitLab features)


Recurring Weekly:
1. [ ] `1/2` Connect with at least 2 Customers each week (Focus: API Users)
1. [X] 1 PM Peer Interview per week
1. [ ] Test at least 1 new integration each week

## Week of 2022-04-25

This week:
1. [ ] `Started` DVCS Deprecation Planning 
1. [ ] `Started` Finish organization of Slack Epic / Share Dovetail insights
1. [X] Share API User Survey findings & Gap Analysis to Working Group
1. [ ] Document 2-3 ServiceNow Use Cases and validate with 2-3 customers (consider some low fidelity designs)
1. [ ] `Started` Review existing API Observability tools/issues, document what we have and how we could use it, and create a gap analysis of what is missing
1. [ ] Propose a prioritization schema for API Working Group
1. [ ] Round out questions and share issue on Marketplaces/Plugins with team
1. [ ] Explore a few VSM Tools
1. [ ] Create my own high-level view of progress (similar to Work in Progress Epics, or leveraging other GitLab features)
1. [ ] `Started` Create skeleton Slides for Ecosystem:Integrations Calls


Recurring Weekly:
1. [ ] Connect with at least 2 Customers each week
1. [X] 1 PM Peer Interview per week - Dov
1. [X] Test at least 1 new integration each week - Tried a few other Slack Apps like Toast

## Week of 2022-04-19

This week:
1. [ ] `Started` Finish organization of Slack Epic / Share Dovetail insights
1. [X] Close API User Survey and create a Presentation to share findings
1. [X] Jumpstart on %15.1
1. [ ] Document 2-3 ServiceNow Use Cases and validate with 2-3 customers (consider some low fidelity designs)
1. [ ] `Started` DVCS Deprecation Planning 
1. [ ] `Started` Review existing API Observability tools/issues, document what we have and how we could use it, and create a gap analysis of what is missing
1. [ ] Propose a prioritization schema for API Working Group
1. [ ] Round out questions and share issue on Marketplaces/Plugins with team
1. [ ] Explore a few VSM Tools
1. [ ] Create my own high-level view of progress (similar to Work in Progress Epics, or leveraging other GitLab features)
1. [ ] `Started` Create skeleton Slides for Ecosystem:Integrations Calls


Recurring Weekly:
1. [X] [1/2] Connect with at least 2 Customers each week
1. [ ] 1 PM Peer Interview per week
1. [X] Test at least 1 new integration each week - tested Auth in Postman

## Week of 2022-03-28

_Note: Short week, only in the office two days._

This week:
1. [ ] `Started` Finish organization of Slack Epic / Share Dovetail insights
1. [ ] `50%` Test ServiceNow Integration and provide feedback; create a walkthrough to discuss areas of improvement
1. [ ] Follow-up on Alliance GTM Opportunity they shared

Recurring Weekly:
1. [ ] `0/1` Connect with at least 2 Customers each week (just 1 this week)
1. [ ] 1 PM Peer Interview per week
1. [ ] Test at least 1 new integration each week

## Week of 2022-03-21

This week:
1. [ ] `Started` Finish organization of Slack Epic / Share Dovetail insights
1. [ ] `Started` Review existing API Observability tools/issues, document what we have and how we could use it, and create a gap analysis of what is missing
1. [ ] `Started` Create skeleton Slides for Ecosystem:Integrations Calls
1. [ ] `50%` Test ServiceNow Integration and provide feedback; create a walkthrough to discuss areas of improvement
1. [ ] Propose a prioritization schema for API Working Group
1. [ ] Round out questions and share issue on Marketplaces/Plugins with team
1. [ ] Explore a few VSM Tools
1. [ ] Create my own high-level view of progress (similar to Work in Progress Epics, or leveraging other GitLab features)

Recurring Weekly:
1. [X] `2/2` Connect with at least 2 Customers each week
1. [ ] 1 PM Peer Interview per week
1. [X] Test at least 1 new integration each week - ServiceNow

## Week of 2022-03-14
1. [X] Finalize API User Survey [so close!]
1. [X] Finalize %14.10 Planning
1. [X] %14.10 Kickoff Video
1. [X] %14.10 PI Review
1. [ ] Finish organization of Slack Epic / Share Dovetail insights
1. [ ] `Started` Review existing API Observability tools/issues, document what we have and how we could use it, and create a gap analysis of what is missing
1. [ ] `Started` Create skeleton Slides for Ecosystem:Integrations Calls
1. [ ] Propose a prioritization schema for API Working Group
1. [X] `3/2` Connect with at least 2 Customers each week
1. [ ] Schedule 2 PM Peer Interviews
1. [ ] Test at least 1 new integration each week
1. [ ] Explore a few VSM Tools
1. [ ] Create my own high-level view of progress (similar to Work in Progress Epics, or leveraging other GitLab features)
1. [ ] Round out questions and share issue on Marketplaces/Plugins with team

## Week of 2022-03-07
1. [99%] Finalize API User Survey
1. [75%] Finalize %14.10 Planning
1. [75%] Complete organization of Slack Epic / Share Dovetail insights
1. [ ] `Started` Review existing API Observability tools/issues, document what we have and how we could use it, and create a gap analysis of what is missing
1. [ ] `Started` Create skeleton Slides for Ecosystem:Integrations Calls
1. [ ] Propose a prioritization schema for API Working Group
1. [X] [2/2] Connect with at least 2 Customers each week
1. [1/2] Schedule 2 PM Peer Interviews
1. [X] Test at least 1 new integration each week - ServiceNow+GitLab Spoke, Jira Group-level integration
1. [ ] Explore a few VSM Tools
1. [ ] Create my own high-level view of progress (similar to Work in Progress Epics, or leveraging other GitLab features)
1. [ ] Round out questions and share issue on Marketplaces/Plugins with team

## Week of 2022-02-28
1. [ ] Complete organization of Slack Epic
1. [ ] `Started` Review existing API Observability tools/issues, document what we have and how we could use it, and create a gap analysis of what is missing
1. [ ] `Started` Create skeleton Slides for Ecosystem:Integrations Calls
1. [ ] Propose a prioritization schema for API Working Group
1. [1/2] Connect with at least 2 Customers each week
1. [1/2] Schedule 2 PM Peer Interviews
1. [ ] Test at least 1 new integration each week
1. [ ] Explore a few VSM Tools
1. [ ] Create my own high-level view of progress (similar to Work in Progress Epics, or leveraging other GitLab features)

## Week of 2022-02-21
1. [1/2] Connect with at least 2 Customers each week
1. [2/1] Test at least 1 new integration each week - tested Figma and Mattermost
1. [X] Minor prep for %14.10
1. [1/2] Review CI/CD Documentation and learn more about the existing features, APIs, and roadmap
1. [95%] Complete API User Survey and implement in API Docs
1. [ ] Schedule 2 PM Peer Interviews
1. [X] Update Jira Docs based on my run through
1. [ ] Propose a prioritization schema for API Working Group
1. [ ] `Started` Review existing API Observability tools/issues, document what we have and how we could use it, and create a gap analysis of what is missing
1. [ ] `Started` Create skeleton Slides for Ecosystem:Integrations Calls
1. [ ] Create my own high-level view of progress (similar to Work in Progress Epics, or leveraging other GitLab features)

## Week of 2022-02-14
1. [X] Merge Direction Updates once comments are closed
1. [1/2] Connect with 2 Customers to capture Slack Integration Feedback
1. [1/2] Refine, clean up, and prioritize Slack epics/issues
1. [ ] Minor prep for %14.10
1. [ ] Review CI/CD Documentation and learn more about the existing features, APIs, and roadmap
1. [ ] Schedule PM Peer Interviews
1. [ ] API Working Group - Iterate on issues and plan actions
1. [X] Work on Survey for API Working Group
1. [X] Product Activities - PI Issue, Potential Kickoff Video

## Week of 2022-01-31
1. [X] Direction Updates following Alliance Team Whiteboarding Sessions & Review of Vision/Strategy Draft Issue
1. [ ] Iterate on Top 20 Partner List and supporting data
1. [X] Deeper research/analysis on current Slack Integration / Customer feedback
1. [ ] Deeper research/analysis on current Jira Integration / Customer feedback
1. [X] Identify Next steps with Community Contributions / Partner Opportunities (TabNine, Dynatrace)

## Week of 2022-01-24
1. [x] Complete Vision/Strategy Draft Issue
1. [x] Share Vision/Strategy Draft Issue for Feedback
1. [ ] Direction Updates following Alliance Team Whiteboarding Sessions & Review of Vision/Strategy Draft Issue
1. [x] Share feedback on the API Working Group issue and work to schedule first meeting
1. [ ] Deeper research/analysis on current Slack Integration / Customer feedback
1. [ ] Deeper research/analysis on current Jira Integration / Customer feedback
1. [ ] Identify Next steps with Community Contributions / Partner Opportunities (TabNine, Dynatrace)

## Week of 2022-01-17

1. [x] Whiteboard Session with Alliance Team to align on Ecosystem::Integrations Vision/Strategy
1. [ ] Direction Updates following Alliance Team Whiteboarding Sessions 
1. [ ] Share feedback on the API Working Group issue and work to schedule first meeting
1. [x] Update OKRS
1. [x] Team Discussion on Slack Integration approach (Self-Managed or .com first)
1. [ ] Deeper research/analysis on current Slack Integration / Customer feedback
1. [ ] Deeper research/analysis on current Jira Integration / Customer feedback
1. [ ] Identify Next steps with Community Contributions / Partner Opportunities (TabNine, Dynatrace)



