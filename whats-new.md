# What's New?

Welcome to my dedicated What's New page! I'll share videos here that help to answer common questions I recieve or explain common topics.

Have questions for me? Curious what we're working on? Wondering what's next? You may be able to find your answers here!

Note: Private/internal videos are shared in GitLab Unfiltered. Learn how to access them [here](https://www.youtube.com/watch?v=dZtCuOf5aGk) if you have issues!

1. `Feb 14, 2025` [Security Policy YAML Validator App](https://security-policies-yaml-schema-validator-862245.gitlab.io/)
   - This is an unofficial support tool for validating YAML _by version_ to support customers
1. `Feb 7, 2025` [Security and Compliance: Notifications/Alerts Research](https://www.youtube.com/watch?v=aY-0tjvSAFA&feature=youtu.be)
1. `Jan 31, 2025` Pipeline execution policies - custom stages for inject mode
   - [POC Demo](https://www.youtube.com/watch?v=ASXp89wyvHQ)
   - [POC Demo: Special cases](https://www.youtube.com/watch?v=1wMOiYnJEJs)
1. `Jan 25, 2025` See Dan's blog post on [Advanced Use Cases for Pipeline Execution Policies](https://about.gitlab.com/blog/2025/01/22/tutorial-advanced-use-case-for-gitlab-pipeline-execution-policies/)
1. `Nov 28, 2024` [Pipeline execution policies "Override" mode now reads variables from `.gitlab-ci.yml`](https://youtu.be/W8tubneJ1X8)
1. `Oct 22, 2024` [Roadmap plans: Exclude Packages in License Approval Policies](https://youtu.be/aBkVIr8l8Nk)
1. `Sep 21, 2024` [Security Policies 3 Year Direction](https://www.youtube.com/watch?v=Kk6eAaUobG4)
1. `Aug 14, 2024` [Pipeline Execution Policies | Use before_script and customize a scanner template](https://youtu.be/_WRUd0zcUnI)
1. `Aug 13, 2024` [Assuring Compliance with Pipeline Execution Policies (by Fern)](https://www.youtube.com/watch?v=kQYeg2sZNHQ)
1. `Jul 20, 2024` [Pipeline Execution Policy - Live Demo](https://youtu.be/UIy17h_73Pc)
1. `Jun 22, 2024` [Pipeline Execution Policy Type](https://youtu.be/QQAOpkZ__pA)
   - Covers the updated plan for the pipeline execution policy, including MVC release scope and planned improvements
1. `Mar 11, 2024` [Compliance Management Direction](https://www.youtube.com/watch?v=5Tv4RGj-msw)
1. `Feb 20, 2024` [Customer Success Skills Exchange - Pipeline Execution Policies](https://gitlab.highspot.com/items/65d514d664cd5581000201ea)
1. [Enablement Bite | What are security and compliance policies?](https://youtu.be/FbU110OpLw8)
1. [Enablement Bite | How can I manage policies globally across a GitLab instance or namespace?](https://youtu.be/WCf7KnXigSc)
1. [Enablement Bite | What's happening with compliance pipelines?](https://youtu.be/7sM0miNHxgs)
1. [Demo of experimental features "policy scoping and pipeline execution action"](https://youtu.be/wxh7BudB_fk)
   - [Feedback Issue](https://gitlab.com/gitlab-org/gitlab/-/issues/434425)
   - [Analyze behaviors and contribute your CI expertise to help us improve!](https://gitlab.com/groups/gitlab-org/-/epics/7312#requirements-matrix)
1. [Another demo of these features with our internal Security Assurance team](https://youtu.be/Rubxji2XB1Y)
   - This demo covers MR approval policies, pipeline execution, policy scoping, and some discussion on compliance frameworks/permissions
1. [Govern Stage Roadmap Review | FY25 Q1](https://youtu.be/U8ggALw7Vmc)
   - Focused on our plan in Govern for the next 3 months
1. [Security Policies FY25 Roadmap](https://youtu.be/AWJd9cpSn-E)
   - Discussion/review of Security Policies plans for roadmap from Feb 2024 - Jan 2025 
   - [FigJam Link](https://www.figma.com/file/aCEICYivtMczzi8LsTglBt/Security-Policies---FY25-Product-Roadmap?type=whiteboard&node-id=65-1876&t=hjX7Vpz9pFABjXMY-4)
